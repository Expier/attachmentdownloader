﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonLibraries.Common;

namespace CommonLibraries.Scrapers
{
    public class CredenaScraper : Watchdog, IScraper
    {
        //first page constants
        private const string LOGIN_INPUT_ID = "ctl00_ctl00_bodyPH_bodyPH_textEmail";
        private const string PASSWORD_INPUT_ID = "ctl00_ctl00_bodyPH_bodyPH_textPassword";
        private const string LOGIN_BUTTON_ID = "ctl00_ctl00_bodyPH_bodyPH_ButtonPassword";

        //second page constants
        private const string DOWNLOAD_LINK_TEXT = "Download";

        public string Login { get; private set; }

        public string Password { get; private set; }

        public event CompletedDelegate ScrapingCompleted;

        private ILog Log
        {
            get { return Logger.Log; }
        }

        private int PageNumber = 0;

        public CredenaScraper(string login, string password)
        {
            Log.Info("Starting new instance of {0}", GetType().Name);

            Login = login;
            Password = password;

            WatchdogElapsed += () => { EndScraping(false, "Request time out"); }; //signal for request timeout
        }


        public void DocumentCompletedHandler(object sender, System.Windows.Forms.WebBrowserDocumentCompletedEventArgs e)
        {
            WatchdogReset();

            Log.Info("Page {0} loaded", PageNumber);

            switch (PageNumber)
            {
                case 0:
                    PageNumber++;
                    //do nothing
                    break;

                case 1:
                    PageNumber++;

                    Task.Run(() =>
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseFirstPage)
                        );
                    break;

                case 2:
                    PageNumber++;
                    //do nothing
                    break;

                case 3:
                    PageNumber++;

                    Task.Run(() =>
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseSecondPage)
                        );
                    break;

                default:
                    string message = String.Format("{0}: Unknown page number {1}!", GetType().Name, PageNumber);

                    Log.Warn(message);

                    if (ScrapingCompleted != null)
                        ScrapingCompleted(false, message);
                    return;
            }
        }

        /// <summary>
        /// parse second page
        /// </summary>
        /// <param name="browser"></param>
        /// <returns></returns>
        private bool ParseSecondPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseSecondPage), browser);
            }

            try
            {
                Log.Info("{0}: start parsing second page", GetType().Name);

                if (browser.Document == null)
                {
                    Log.Warn("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                var links = browser.Document.GetElementsByTagName(HtmlTags.a.ToString());

                foreach (HtmlElement link in links)
                {
                    if (String.Compare(link.InnerText, DOWNLOAD_LINK_TEXT) == 0)
                    {
                        Log.Info("{0}: found download link", GetType().Name);

                        //expecting for download dialog to appear
                        (browser).Navigating += HtmlParser.BrowserDownloadEventHandler;

                        HtmlParser.DownloadStarted += WatchdogReset; //reset Watchdog when download starts
                        HtmlParser.DownloadCompleted += () => { EndScraping(true); }; //signal download completed successfully

                        link.InvokeMember("click");

                        Log.Info("{0}: Page parsed successfully", GetType().Name);
                        return true;
                    }
                }

                //if we've reached this point - download link was not found

                Log.Warn("{0}: Download link was not found", GetType().Name);
                return false;
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }
        }

        /// <summary>
        /// Parse login page
        /// </summary>
        /// <param name="browser"></param>
        /// <returns></returns>
        private bool ParseFirstPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseFirstPage), browser);
            }

            try
            {
                Log.Info("{0}: start parsing first page", GetType().Name);

                if (browser.Document == null)
                {
                    Log.Warn("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                var loginInput = browser.Document.GetElementById(LOGIN_INPUT_ID);

                if (loginInput == null)
                {
                    Log.Warn("{0}: element not found: <input id=\"{1}\"", GetType().Name, LOGIN_INPUT_ID);
                    return false;
                }

                loginInput.SetAttribute("Value", Login);

                var passwordInput = browser.Document.GetElementById(PASSWORD_INPUT_ID);

                if (passwordInput == null)
                {
                    Log.Warn("{0}: element not found: <input id=\"{1}\"", GetType().Name, PASSWORD_INPUT_ID);
                    return false;
                }

                passwordInput.SetAttribute("Value", Password);

                var loginButton = browser.Document.GetElementById(LOGIN_BUTTON_ID);

                if (loginButton == null)
                {
                    Log.Warn("{0}: element not found: <button id=\"{1}\"", GetType().Name, LOGIN_BUTTON_ID);
                    return false;
                }

                loginButton.Enabled = true;
                loginButton.InvokeMember("click");
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        /// <summary>
        /// Siganl all waiters that scrapping is completed
        /// </summary>
        /// <param name="succeeded"></param>
        /// <param name="result"></param>
        private void EndScraping(bool succeeded, string result = null)
        {
            try
            {
                WatchdogStop();

                if (ScrapingCompleted == null)
                    return;

                ScrapingCompleted(succeeded, result);
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("{0}: Error occured: {1}", this.GetType().Name, ex));
            }
        }
    }
}
