﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonLibraries.Common;

namespace CommonLibraries.Scrapers
{
    public class CostcoScraper: Watchdog, IScraper
    {
        //first page constants
        private const string DIV_SUBMIT_ID = "submitbutton";
        private const string INPUT_LOGIN_ID = "loginname";
        private const string INPUT_PASSWORD_ID = "password";

        //second page constatnts
        private const string MAIL_SENDER_CLASS = "mailsender";

        //third page constants
        private const string ATTACHMENT_DIV_ID = "messageattachment";
        
        public string Login { get; private set; }

        public string Password { get; private set; }

        public event CompletedDelegate ScrapingCompleted;

        private ILog Log
        {
            get { return Logger.Log; }
        }

        private int PageNumber = 0;

        public CostcoScraper(string login, string password)
        {
            Log.Info("Starting new instance of {0}", GetType().Name);

            Login = login;
            Password = password;

            WatchdogElapsed += () => { EndScraping(false, "Request time out"); }; //signal for request timeout
        }
        
        public void DocumentCompletedHandler(object sender, System.Windows.Forms.WebBrowserDocumentCompletedEventArgs e)
        {
            WatchdogReset();

            Log.Info("Page {0} loaded", PageNumber);

            switch (PageNumber)
            {
                case 0:
                    Task.Run(() =>
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseFirstPage)
                        );
                    break;

                case 1:
                    //do nothing
                    break;

                case 2:
                    Task.Run(() =>
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseSecondPage)
                        );
                    break;

                case 3:
                    Task.Run(() =>
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseThirdPage)
                        );
                    break;

                default:
                    string message = String.Format("{0}: Unknown page number {1}!", GetType().Name, PageNumber);

                    Log.Warn(message);

                    EndScraping(false, message);
                    return;
            }

            PageNumber++;
        }

        private bool ParseThirdPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseThirdPage), browser);
            }

            try
            {
                Log.Info("{0}: start parsing a page", GetType().Name);

                if (browser.Document == null)
                {
                    Log.Warn("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }
                
                //find clicable element
                var attachmentsDiv = browser.Document.GetElementById(ATTACHMENT_DIV_ID);
                if (attachmentsDiv == null)
                {
                    Log.Warn("Unable to find element: <div id=\"{0}\"", attachmentsDiv);
                    return false;
                }

                var attachmentLinks = attachmentsDiv.GetElementsByTagName(HtmlTags.a.ToString());

                if (attachmentLinks.Count < 1)
                {
                    Log.Warn("Unable to find element: <a href=");
                    return false;
                }

                //expecting for download dialog to appear
                (browser).Navigating += HtmlParser.BrowserDownloadEventHandler;

                //unsubscribe from event
                (browser).DocumentCompleted -= DocumentCompletedHandler;
                
                HtmlParser.DownloadStarted += WatchdogReset; //reset Watchdog when download starts
                HtmlParser.DownloadCompleted += () => { EndScraping(true); }; //signal download completed successfully

                foreach (HtmlElement attachmentLink in attachmentLinks)
                {
                    var url = attachmentLink.GetAttribute("href");
                    browser.Navigate(url);
                }
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        private bool ParseSecondPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseSecondPage), browser);
            }

            try
            {
                Log.Info("{0}: start parsing a page", GetType().Name);

                if (browser.Document == null)
                {
                    Log.Warn("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                //find clicable element
                var mailSenders = HtmlParser.FindElementByClassName(browser, HtmlTags.li, MAIL_SENDER_CLASS);
                if (mailSenders == null || mailSenders.Count < 1)
                {
                    Log.Warn("Unable to find element: <li class=\"{0}\"", MAIL_SENDER_CLASS);
                    return false;
                }

                mailSenders[0].InvokeMember("click");
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        private bool ParseFirstPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseFirstPage), browser);
            }

            try
            {
                Log.Info("{0}: start parsing a page", GetType().Name);

                if (browser.Document == null)
                {
                    Log.Warn("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                //find login input
                var inputLogin = browser.Document.GetElementById(INPUT_LOGIN_ID);
                if (inputLogin == null)
                {
                    Log.Warn("Unable to find element: <input id=\"{0}\"", INPUT_LOGIN_ID);
                    return false;
                }

                inputLogin.SetAttribute("Value", Login);

                //find password input
                var inputPassword = browser.Document.GetElementById(INPUT_PASSWORD_ID);
                if (inputPassword == null)
                {
                    Log.Warn("Unable to find element: <input id=\"{0}\"", INPUT_PASSWORD_ID);
                    return false;
                }
                inputPassword.SetAttribute("Value", Password);

                //find submit div
                var divSubmit = browser.Document.GetElementById(DIV_SUBMIT_ID);

                if (divSubmit == null)
                {
                    Log.Warn("Unable to find element: <div id=\"{0}\"", DIV_SUBMIT_ID);
                    return false;
                }

                //find submit button
                var submitButton = divSubmit.GetElementsByTagName(HtmlTags.input.ToString())[0];
                submitButton.InvokeMember("click");
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        /// <summary>
        /// Siganl all waiters that scrapping is completed
        /// </summary>
        /// <param name="succeeded"></param>
        /// <param name="result"></param>
        private void EndScraping(bool succeeded, string result = null)
        {
            try
            {
                WatchdogStop();

                if (ScrapingCompleted == null)
                    return;

                ScrapingCompleted(succeeded, result);
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("{0}: Error occured: {1}", this.GetType().Name, ex));
            }
        }
    }
}
