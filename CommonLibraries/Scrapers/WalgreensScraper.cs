﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonLibraries.Common;
using mshtml;

namespace CommonLibraries.Scrapers
{
    public class WalgreensScraper : Watchdog, IScraper
    {
        private const string InputPasswordId = "passwordInput1";
        private const string OpnOnlineLinkId = "openOnlineLink";
        private const string OpnOnlineBtnId = "text_i18n.authframe.safr.button.openonline";

        private const string emailSelect = "toSelect";
        private const string btnSubmitId = "text_buttonSubmit";
        

        private Dictionary<string, string> attachmentsList = new Dictionary<string, string>();
        private ManualResetEvent WaitForFileDownload;

        public string Login { get; private set; }

        public string Password { get; private set; }

        public event CompletedDelegate ScrapingCompleted;


        private ILog Log
        {
            get { return Logger.Log; }
        }

        //for this scraper PageNumber starts from -1
        private int PageNumber = -1;

        public WalgreensScraper(string login, string password)
        {
            Log.Info("Starting new instance of {0}", GetType().Name);

            Login = login;
            Password = password;

            WatchdogElapsed += () => { EndScraping(false, "Request time out"); }; //signal for request timeout
        }

        //indicates if the Second Page's parser is already started
        private bool SecondPageParsingIsStarted = false;

        public void DocumentCompletedHandler(object sender, System.Windows.Forms.WebBrowserDocumentCompletedEventArgs e)
        {
            WatchdogReset();

            Log.Info("Page {0} loaded", PageNumber);

            switch (PageNumber)
            {
                case -1:
                    Task.Run(() =>
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParsePreAuthPage)
                        );
                    break;

                case 0:
                    Task.Run(() =>
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseFirstPage)
                        );
                    break;

                case 1:
                case 2:
                    if (!SecondPageParsingIsStarted)
                    {
                        SecondPageParsingIsStarted = true;

                        Task.Run(() =>
                            HtmlParser.ParsePageInLoop((WebBrowser) sender, ParseSecondPage)
                            );
                    }
                    break;

                default:
                    string message = String.Format("{0}: Unknown page number {1}!", GetType().Name, PageNumber);

                    Log.Warn(message);

                    EndScraping(false, message);
                    return;
            }

            PageNumber++;
        }

        private bool ParseSecondPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseSecondPage), browser);
            }

            try
            {
                Log.Info("{0}: start parsing a page", GetType().Name);

                if (browser.Document == null)
                {
                    Log.Warn("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                var srForm = browser.Document.GetElementById("srform");

                var innerTbl0 = srForm.GetElementsByTagName("table")[0];
                var innerTbl1 = innerTbl0.GetElementsByTagName("table")[0];

                var tr = innerTbl1.GetElementsByTagName("tr")[3];
                var targetTbl = tr.GetElementsByTagName("table")[0];

                var targetTr = targetTbl.GetElementsByTagName("tr")[4];
                var targetTd = targetTr.GetElementsByTagName("td")[1];

                var links = targetTd.GetElementsByTagName("a");

                if (links.Count < 1)
                {
                    targetTr = targetTbl.GetElementsByTagName("tr")[5];
                    targetTd = targetTr.GetElementsByTagName("td")[1];

                    links = targetTd.GetElementsByTagName("a");
                }

                foreach (HtmlElement link in links)
                {
                    attachmentsList.Add(link.InnerText, link.GetAttribute("href"));
                }

                ////expecting for download dialog to appear
                //browser.Navigating += HtmlParser.BrowserDownloadEventHandler;
                //    //BrowserDownloadAttachmentHandler;

                ////unsubscribe from event
                //browser.DocumentCompleted -= DocumentCompletedHandler;

                //while (attachmentsList.Count > 0)
                //{
                //    browser.Navigate(attachmentsList.First().Value);
                //}

                //expecting for download dialog to appear
                (browser).Navigating += HtmlParser.DwonloadHandlerWebrequest;

                //unsubscribe from event
                (browser).DocumentCompleted -= DocumentCompletedHandler;

                HtmlParser.DownloadStarted += WatchdogReset; //reset Watchdog when download starts
                HtmlParser.DownloadCompleted += () => { EndScraping(true); }; //signal download completed successfully

                foreach (HtmlElement link in links)
                {
                    var url = link.GetAttribute("href");
                    browser.Navigate(url);
                }
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        private void BrowserDownloadAttachmentHandler(object sender, WebBrowserNavigatingEventArgs e)
        {
            if (String.Compare(e.Url.AbsolutePath, "false") == 0)
                return;

            e.Cancel = true;

            WebBrowser br = ((WebBrowser)sender);

            if (br.Document == null)
                return;

            CookieAwareWebClient webClient = new CookieAwareWebClient(CookieAwareWebClient.GetUriCookieContainer(e.Url));
            //webClient.Headers.Add(HttpRequestHeader.Cookie, br.Document.Cookie);
            webClient.DownloadDataCompleted += webClient_DownloadDataCompleted;

            WatchdogReset();

            Log.Info("Starting file download");

            webClient.DownloadDataAsync(e.Url);
        }

        private void webClient_DownloadDataCompleted(object sender, DownloadDataCompletedEventArgs e)
        {
            try
            {
                WatchdogReset();

                string filepath = Application.StartupPath;

                string filename = attachmentsList.First().Key;

                File.WriteAllBytes(Path.Combine(filepath, filename), e.Result);

                Log.Info("File download completed.");
            }
            catch (Exception ex)
            {
                Log.Warn("Failed to download file");
                Log.Error("webClient_DownloadDataCompleted: error occured: {0}", ex);
            }
            finally
            {
                //WaitForFileDownload.Set();
                attachmentsList.Remove(attachmentsList.First().Key);
            }
        }

        private bool ParsePreAuthPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParsePreAuthPage), browser);
            }

            try
            {
                Log.Info("{0}: start parsing a page", GetType().Name);

                if (browser.Document == null)
                {
                    Log.Warn("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                var recepientSelect = browser.Document.GetElementById(emailSelect);

                if (recepientSelect == null)
                {
                    //go to next page
                    PageNumber++;

                    HtmlParser.ParsePageInLoop(browser, ParseFirstPage);

                    return true;
                }

                foreach (HtmlElement option in recepientSelect.Children)
                {
                    if (String.Compare(option.GetAttribute("value").ToLower(), Login.ToLower()) == 0)
                    {
                        option.SetAttribute("selected", "selected");
                        break;
                    }
                }

                var submitBtn = browser.Document.GetElementById(btnSubmitId);

                submitBtn.InvokeMember("Click");
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        private bool ParseFirstPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseFirstPage), browser);
            }

            try
            {
                Log.Info("{0}: start parsing a page", GetType().Name);

                var iFrame = HtmlParser.GetDocumentFromWindow((IHTMLWindow2)browser.Document.Window.Frames[0].DomWindow);

                var inputPassword = ((HTMLDocument)iFrame).getElementById(InputPasswordId);
                inputPassword.setAttribute("value", Password);

                var opnOnline = ((HTMLDocument)iFrame).getElementById(OpnOnlineLinkId);

                if (opnOnline == null)
                    opnOnline = ((HTMLDocument)iFrame).getElementById(OpnOnlineBtnId);

                opnOnline.click();
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        private bool RecepientSelectAuth(WebBrowser browser)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        /// <summary>
        /// Siganl all waiters that scrapping is completed
        /// </summary>
        /// <param name="succeeded"></param>
        /// <param name="result"></param>
        private void EndScraping(bool succeeded, string result = null)
        {
            try
            {
                WatchdogStop();

                if (ScrapingCompleted == null)
                    return;

                ScrapingCompleted(succeeded, result);
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("{0}: Error occured: {1}", this.GetType().Name, ex));
            }
        }
    }
}
