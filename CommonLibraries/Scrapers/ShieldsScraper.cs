﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonLibraries.Common;
using Timer = System.Timers.Timer;
using System.Net.Mime;

namespace CommonLibraries.Scrapers
{
    public class ShieldsScraper : IScraper
    {
        private const string LOGIN_INPUT_NAME = "ctl00$ContentHolder$Email";
        private const string PASSWORD_INPUT_NAME = "ctl00$ContentHolder$Password";
        private const string LOGIN_LINK_ID = "ctl00_ContentHolder_LoginButton";

        private const string DOWNLOAD_BUTTON_ID = "download-btn";

        //Watchdog timer interval, milliseconds
        private const int WATCHDOG_INTERVAL = 10 * 1000;

        private ILog Log
        {
            get { return Logger.Log; }
        }

        //public string Url { get; private set; }
        public string Login { get; private set; }
        public string Password { get; private set; }

        public event CompletedDelegate ScrapingCompleted;

        public WebBrowser WebBrowser { get; private set; }



        //Number of pages navigated since initial URL
        private int pageNumber = 0;

        //Timer watching ower process execution
        private Timer WatchdogTimer;

        //public ShieldsScraper(string url, string login, string password)
        public ShieldsScraper(string login, string password)
        {
            Log.Info(String.Format("Creating instance of {0}", this.GetType().Name));

            //Url = url;
            Login = login;
            Password = password;

            WatchdogTimer = new Timer(WATCHDOG_INTERVAL);
            WatchdogTimer.AutoReset = false;
            WatchdogTimer.Elapsed += WatchdogTimer_Elapsed;
        }

        private void ResetWatchdogTimer()
        {
            if (WatchdogTimer != null)
            {
                WatchdogTimer.Stop();
                WatchdogTimer.Start();
            }
        }

        void WatchdogTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            EndScraping(false, String.Format("Scraping completed by timeout: {0}", WATCHDOG_INTERVAL));
        }

        //private WebBrowser webBrowser;
        public void DocumentCompletedHandler(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            ResetWatchdogTimer();

            switch (pageNumber)
            {
                case 0: //we are on a first page (initial url)
                    //ParseFirstPage((WebBrowser)sender);    
                    //run page parsing asyncronously
                    Task.Run(() =>
                    {
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseFirstPage);
                    });
                    break;

                //case 1:
                //    //do nothing
                //    break;

                case 1: //we are on a second page. Login is expected to be successful
                    //run page parsing asyncronously
                    Task.Run(() =>
                    {
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseSecondPage);
                    });
                    break;

                default:
                    var message = String.Format("{0}: Unknown page number {1}", GetType().Name, pageNumber);
                    Log.Warn(message);

                    //EndScraping(false, message);
                    break;
            }

            pageNumber++;
        }

        void HtmlParser_DownloadCompleted()
        {
            EndScraping(true);
        }

        void HtmlParser_DownloadStarted()
        {
            ResetWatchdogTimer();
        }

        /// <summary>
        /// Scrape second page
        /// </summary>
        /// <param name="browser"></param>
        /// <returns></returns>
        private bool ParseSecondPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseSecondPage), browser);
            }

            try
            {
                if (browser.Document == null)
                {
                    Log.Warn("{0} browser.Document is empty", GetType().Name);
                    return false;
                }

                HtmlElement downloadButton = browser.Document.GetElementById(DOWNLOAD_BUTTON_ID);

                if (downloadButton == null)
                {
                    Log.Warn("{0}: Download button not found <button id=\"{1}\"", GetType().Name, DOWNLOAD_BUTTON_ID);
                    return false;
                }


                //expecting for download dialog to appear
                browser.Navigating += HtmlParser.BrowserDownloadEventHandler;

                HtmlParser.DownloadStarted += HtmlParser_DownloadStarted;
                HtmlParser.DownloadCompleted += HtmlParser_DownloadCompleted;


                downloadButton.InvokeMember("click");
            }
            catch (Exception ex)
            {
                Log.Error("Error occured: {0}", ex);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Parse First page
        /// </summary>
        /// <param name="browser"></param>
        /// <returns></returns>
        public bool ParseFirstPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseFirstPage), browser);
            }

            try
            {
                if (browser.Document == null)
                {
                    Log.Error("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                //get all inputs from page
                var inputs = browser.Document.GetElementsByTagName(HtmlTags.input.ToString());

                //if (inputs.Count < 1)
                //{
                //    Log.Error("{0}: Input elements not found", GetType().Name);
                //    return false;
                //}

                //find login input by name
                var loginInput = inputs.GetElementsByName(LOGIN_INPUT_NAME);

                //if (loginInput.Count < 1)
                //{
                //    Log.Error("{0}: Input elements not found <input name=\"{1}\"", GetType().Name, LOGIN_INPUT_NAME);
                //    return false;
                //}

                //set value for login
                loginInput[0].SetAttribute("Value", Login);
                //loginInput[0].InnerText = Login;

#if DEBUG
                Console.WriteLine("Entering Login");
#endif

                //find login input by name
                var passwordInput = inputs.GetElementsByName(PASSWORD_INPUT_NAME);

                //if (passwordInput.Count < 1)
                //{
                //    Log.Error("{0}: Input elements not found <input name=\"{1}\"", GetType().Name, PASSWORD_INPUT_NAME);
                //    return false;
                //}

                passwordInput[0].SetAttribute("Value", Password);
                //passwordInput[0].InnerText = Password;

#if DEBUG
                Console.WriteLine("Entering Password");
#endif

                //get "login" link
                var link = browser.Document.GetElementById(LOGIN_LINK_ID);

                //if (link == null)
                //{
                //    Log.Error("{0}: Login link not found <a id=\"{1}\"", GetType().Name, LOGIN_LINK_ID);
                //    return false;
                //}

                link.InvokeMember("click");

#if DEBUG
                Console.WriteLine("Clicking link");
#endif
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("{0}: Error occured: {1}", GetType().Name, ex));
                return ParseSecondPage(browser);
            }

            return true;
        }

        /// <summary>
        /// Siganl all waiters that scrapping is completed
        /// </summary>
        /// <param name="succeeded"></param>
        /// <param name="result"></param>
        private void EndScraping(bool succeeded, string result = null)
        {
            try
            {
                if (ScrapingCompleted == null)
                    return;

                ScrapingCompleted(succeeded, result);
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("{0}: Error occured: {1}", this.GetType().Name, ex));
            }
        }
    }
}
