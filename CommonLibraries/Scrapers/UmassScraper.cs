﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonLibraries.Common;
using Timer = System.Timers.Timer;
using System.Net.Mime;
using System.Runtime.InteropServices.WindowsRuntime;

namespace CommonLibraries.Scrapers
{
    public class UmassScraper : Watchdog, IScraper
    {
        private const string InputLoginId = "credentials-email";
        private const string InputPasswordId = "credentials-password";
        private const string BtnLoginId = "start-button";

        private const string BtnDownloadId = "download-btn";

        //Watchdog timer interval, milliseconds
        private const int WATCHDOG_INTERVAL = 10 * 1000;

        private ILog Log
        {
            get { return Logger.Log; }
        }

        //public string Url { get; private set; }
        public string Login { get; private set; }
        public string Password { get; private set; }

        public event CompletedDelegate ScrapingCompleted;

        public WebBrowser WebBrowser { get; private set; }



        //Number of pages navigated since initial URL
        private int PageNumber = 0;

        public UmassScraper(string login, string password)
        {
            Log.Info(String.Format("Creating instance of {0}", this.GetType().Name));

            //Url = url;
            Login = login;
            Password = password;

            WatchdogElapsed += () => { EndScraping(false, "Request time out"); }; //signal for request timeout
        }

        public void DocumentCompletedHandler(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            WatchdogReset();

            switch (PageNumber)
            {
                case 2:
                    Task.Run(() =>
                    {
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseFirstPage);
                    });
                    break;
                    
                case 3: //we are on a second page. Login is expected to be successful
                    //run page parsing asyncronously
                    Task.Run(() =>
                    {
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseSecondPage);
                    });
                    break;

                default:
                    var message = String.Format("{0}: Unknown page number {1}", GetType().Name, PageNumber);
                    Log.Warn(message);

                    //EndScraping(false, message);
                    break;
            }

            PageNumber++;
        }

        void HtmlParser_DownloadCompleted()
        {
            EndScraping(true);
        }

        void HtmlParser_DownloadStarted()
        {
            WatchdogReset();
        }

        /// <summary>
        /// Scrape second page
        /// </summary>
        /// <param name="browser"></param>
        /// <returns></returns>
        private bool ParseSecondPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseSecondPage), browser);
            }

            try
            {
                if (browser.Document == null)
                {
                    Log.Warn("{0} browser.Document is empty", GetType().Name);
                    return false;
                }

                HtmlElement downloadButton = browser.Document.GetElementById(BtnDownloadId);

                if (downloadButton == null)
                {
                    Log.Warn("{0}: Download button not found <button id=\"{1}\"", GetType().Name, BtnDownloadId);
                    return false;
                }


                //expecting for download dialog to appear
                browser.Navigating += HtmlParser.BrowserDownloadEventHandler;

                HtmlParser.DownloadStarted += HtmlParser_DownloadStarted;
                HtmlParser.DownloadCompleted += HtmlParser_DownloadCompleted;


                downloadButton.InvokeMember("click");
            }
            catch (Exception ex)
            {
                Log.Error("Error occured: {0}", ex);
                return false;
            }

            return true;
        }

        private WebBrowser Browser;

        /// <summary>
        /// Parse First page
        /// </summary>
        /// <param name="browser"></param>
        /// <returns></returns>
        public bool ParseFirstPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseFirstPage), browser);
            }

            try
            {
                if (browser.Document == null)
                {
                    Log.Error("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                //find login input
                var loginInput = browser.Document.GetElementById(InputLoginId);

#if DEBUG
                Console.WriteLine("Entering Login");
#endif
                loginInput.Focus();
                //set value for login
                loginInput.SetAttribute("Value", Login);

                loginInput.RemoveFocus();

#if DEBUG
                Console.WriteLine("Entering Password");
#endif

                //find password input
                var passwordInput = browser.Document.GetElementById(InputPasswordId);

                passwordInput.Focus();

                passwordInput.SetAttribute("Value", Password);

                passwordInput.RemoveFocus();


#if DEBUG
                Console.WriteLine("Clicking link");
#endif

                //click "login" btn
                Task.Run(() =>
                {
                    //wait for a while to let JavaScript do his work
                    Thread.Sleep(2000);

                    ClickLink(browser);
                });
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("{0}: Error occured: {1}", GetType().Name, ex));
                //return ParseSecondPage(browser);
                return false;
            }

            return true;
        }


        private delegate void ClickLinkDelegate(WebBrowser browser);

        private void ClickLink(WebBrowser browser)
        {
            if (browser.InvokeRequired)
            {
                browser.Invoke(new ClickLinkDelegate(ClickLink), browser);
                return;
            }

            var link = browser.Document.GetElementById(BtnLoginId);
            link.Focus();
            link.InvokeMember("click");
            link.RemoveFocus();
        }



        /// <summary>
        /// Siganl all waiters that scrapping is completed
        /// </summary>
        /// <param name="succeeded"></param>
        /// <param name="result"></param>
        private void EndScraping(bool succeeded, string result = null)
        {
            try
            {
                if (ScrapingCompleted == null)
                    return;

                ScrapingCompleted(succeeded, result);
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("{0}: Error occured: {1}", this.GetType().Name, ex));
            }
        }
    }
}
