﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonLibraries.Common;

namespace CommonLibraries.Scrapers
{
    public class LandvoiceScraper : Watchdog, IScraper
    {
        //first page constants
        private const string INPUT_LOGIN_ID = "id_username";
        private const string INPUT_PASSWORD_ID = "id_password";
        private const string INPUT_SUBMIT_TYPE = "submit";

        public string Login { get; private set; }

        public string Password { get; private set; }

        public event CompletedDelegate ScrapingCompleted;

        private ILog Log
        {
            get { return Logger.Log; }
        }

        private int PageNumber = 0;

        public LandvoiceScraper(string login, string password)
        {
            Log.Info("Starting new instance of {0}", GetType().Name);

            Login = login;
            Password = password;

            WatchdogElapsed += () => { EndScraping(false, "Request time out"); }; //signal for request timeout
        }


        public void DocumentCompletedHandler(object sender, System.Windows.Forms.WebBrowserDocumentCompletedEventArgs e)
        {
            WatchdogReset();
            Log.Info("Page {0} loaded", PageNumber);

            switch (PageNumber)
            {
                case 0:
                    Task.Run(() =>
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseFirstPage)
                        );
                    break;
                default:
                    string message = String.Format("{0}: Unknown page number {1}!", GetType().Name, PageNumber);

                    Log.Warn(message);

                    EndScraping(false, message);
                    return;
            }

            PageNumber++;
        }

        private bool ParseFirstPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseFirstPage), browser);
            }

            try
            {
                Log.Info("{0}: start parsing a page", GetType().Name);

                if (browser.Document == null)
                {
                    Log.Warn("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                var inputLogin = browser.Document.GetElementById(INPUT_LOGIN_ID);
                if (inputLogin == null)
                {
                    Log.Warn("Unable to find element: <input id=\"{0}\"", INPUT_LOGIN_ID);
                    return false;
                }

                inputLogin.SetAttribute("Value", Login);

                var inputPassword = browser.Document.GetElementById(INPUT_PASSWORD_ID);
                if (inputPassword == null)
                {
                    Log.Warn("Unable to find element: <input id=\"{0}\"", INPUT_PASSWORD_ID);
                    return false;
                }

                inputPassword.SetAttribute("Value", Password);

                var inputSubmit = HtmlParser.FindElementsByAttribute(browser, HtmlTags.input, HtmlAttributes.Type, INPUT_SUBMIT_TYPE);
                if (inputSubmit == null)
                {
                    Log.Warn("Unable to find element: <input type=\"{0}\"", INPUT_SUBMIT_TYPE);
                    return false;
                }

                //handle download
                ((SHDocVw.WebBrowser)browser.ActiveXInstance).BeforeNavigate2 += LandvoiceScraper_BeforeNavigate2;

                //unsubscribe from event
                (browser).DocumentCompleted -= DocumentCompletedHandler;

                browser.Navigating += Browser_Navigating;

                inputSubmit[0].InvokeMember("click");
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        string navigatingUrl;
        private void Browser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            navigatingUrl = e.Url.ToString();
        }

        void LandvoiceScraper_BeforeNavigate2(object pDisp, ref object URL, ref object Flags, ref object TargetFrameName, ref object PostData, ref object Headers, ref bool Cancel)
        {
            try
            {
                //if (!((string)Headers).Contains("Referer"))
                //    return;

                WatchdogReset();

                //cancel navigation
                Cancel = true;
                
                var byteArray = PostData as byte[];
                
                //get web request prepared
                HttpWebRequest webRequest = HtmlParser.PreapreWebRequest(URL.ToString(), byteArray, Headers.ToString(), navigatingUrl);

                using (Stream requeStream = webRequest.GetRequestStream())
                {
                    //open connection
                    requeStream.Write(byteArray, 0, byteArray.Length); // Send the data.
                }

                //try to get file from response adn save it
                HtmlParser.SaveFileFromResponse((HttpWebResponse)webRequest.GetResponse());
                
                EndScraping(true);
            }
            catch (Exception ex)
            {
                Log.Error("{0}: error occured: {1} {2} {3}", GetType().Name, ex, Environment.NewLine 
                    ,String.Format("pDisp: {0} URL: {1} Flags: {2} TargetFrameName: {3} PostData {4} Headers {5} Cancel {6}"
                        , pDisp, URL, Flags, TargetFrameName, PostData, Headers, Cancel));
            }
        }



        /// <summary>
        /// Siganl all waiters that scrapping is completed
        /// </summary>
        /// <param name="succeeded"></param>
        /// <param name="result"></param>
        private void EndScraping(bool succeeded, string result = null)
        {
            try
            {
                WatchdogStop();

                if (ScrapingCompleted == null)
                    return;

                ScrapingCompleted(succeeded, result);
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("{0}: Error occured: {1}", this.GetType().Name, ex));
            }
        }
    }
}
