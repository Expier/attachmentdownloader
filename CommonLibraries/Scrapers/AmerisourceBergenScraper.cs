﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonLibraries.Common;

namespace CommonLibraries.Scrapers
{
    public class AmerisourceBergenScraper : Watchdog, IScraper
    {
        #region Login Page Constants

        private const string SUBMIT_BUTTON_ID = "welcome_submit";
        private const string INPUT_LOGIN_ID = "welcome_userName";
        private const string INPUT_PASSWORD_ID = "welcome_password";

        #endregion

        #region File Download Page Constants

        private const string FILE_SENDER_CLASS = "fileDownloadtolocal_submit";
        private const string FILE_CHECKBOX_CLASS = "fileDownload_fileChk";
        private const string FILE_DOWNLOD_OPTION = "fileDownload_downloadOption";

        #endregion

        #region Variables

        private int _pageNumber = 0;

        #endregion

        #region Properties

        private ILog Log
        {
            get { return Logger.Log; }
        }

        public string Login { get; private set; }

        public string Password { get; private set; }

        #endregion

        public event CompletedDelegate ScrapingCompleted;

        public AmerisourceBergenScraper(string login, string password)
        {
            Log.Info("Starting new instance of {0}", GetType().Name);

            Login = login;
            Password = password;

            WatchdogElapsed += () => EndScraping(false, "Request time out"); //signal for request timeout
        }

        public void DocumentCompletedHandler(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            WatchdogReset();

            Log.Info("Page {0} loaded", _pageNumber);

            switch (_pageNumber)
            {
                case 0:
                    Task.Run(() => HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseFirstPage));
                    break;

                case 1:
                    Task.Run(() => HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseSecondPage));
                    break;

                //case 2:
                //    Task.Run(() => HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseSecondPage));
                //    break;

                //case 3:
                //    Task.Run(() => HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseThirdPage));
                //    break;

                default:
                    string message = String.Format("{0}: Unknown page number {1}!", GetType().Name, _pageNumber);

                    Log.Warn(message);

                    EndScraping(false, message);
                    return;
            }

            _pageNumber++;
        }

        /// <summary>
        /// Parse file download page.
        /// </summary>
        /// <param name="browser"></param>
        /// <returns></returns>
        private bool ParseSecondPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseSecondPage), browser);
            }

            try
            {
                Log.Info("{0}: start parsing a page", GetType().Name);

                if (browser.Document == null)
                {
                    Log.Warn("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                //find checkbox elements to select files that should be downloaded
                var element = browser.Document.GetElementById(FILE_CHECKBOX_CLASS);
                if (element == null)
                {
                    Log.Warn("{0}: {1} is empty", GetType().Name, FILE_CHECKBOX_CLASS);
                    return false;
                }
                element.InvokeMember("click");

                //expecting for download dialog to appear
                (browser).Navigating += HtmlParser.BrowserDownloadEventHandler;
                //unsubscribe from event
                (browser).DocumentCompleted -= DocumentCompletedHandler;

                //WatchdogStop();
                HtmlParser.DownloadStarted += WatchdogReset; //reset Watchdog when download starts
                HtmlParser.DownloadCompleted += OnFileDownloadComplete;

                //press file download button
                var fileSenders = browser.Document.GetElementById(FILE_SENDER_CLASS);
                if (fileSenders == null)
                {
                    Log.Warn("Unable to find element: <li class=\"{0}\"", FILE_SENDER_CLASS);
                    return false;
                }
                fileSenders.InvokeMember("click"); 
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        private void OnFileDownloadComplete()
        {
            HtmlParser.DownloadCompleted += () => EndScraping(true);
        }

        /// <summary>
        /// Parse login page.
        /// </summary>
        /// <param name="browser"></param>
        /// <returns></returns>
        private bool ParseFirstPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseFirstPage), browser);
            }

            try
            {
                Log.Info("{0}: start parsing a page", GetType().Name);

                if (browser.Document == null)
                {
                    Log.Warn("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                //find login input
                var inputLogin = browser.Document.GetElementById(INPUT_LOGIN_ID);
                if (inputLogin == null)
                {
                    Log.Warn("Unable to find element: <input id=\"{0}\"", INPUT_LOGIN_ID);
                    return false;
                }
                inputLogin.SetAttribute("Value", Login);

                //find password input
                var inputPassword = browser.Document.GetElementById(INPUT_PASSWORD_ID);
                if (inputPassword == null)
                {
                    Log.Warn("Unable to find element: <input id=\"{0}\"", INPUT_PASSWORD_ID);
                    return false;
                }
                inputPassword.SetAttribute("Value", Password);

                //find submit button
                var submitButton = browser.Document.GetElementById(SUBMIT_BUTTON_ID);
                if (submitButton == null)
                {
                    Log.Warn("Unable to find element: <div id=\"{0}\"", SUBMIT_BUTTON_ID);
                    return false;
                }
                submitButton.InvokeMember("click");
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        /// <summary>
        /// Siganl all waiters that scrapping is completed
        /// </summary>
        /// <param name="succeeded"></param>
        /// <param name="result"></param>
        private void EndScraping(bool succeeded, string result = null)
        {
            try
            {
                WatchdogStop();

                if (ScrapingCompleted == null)
                    return;

                ScrapingCompleted(succeeded, result);
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("{0}: Error occured: {1}", this.GetType().Name, ex));
            }
        }

    }
}
