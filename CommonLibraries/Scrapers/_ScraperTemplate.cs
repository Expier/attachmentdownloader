﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonLibraries.Common;

namespace CommonLibraries.Scrapers
{
    public class _ScraperTemplate : Watchdog, IScraper
    {
        public string Login { get; private set; }

        public string Password { get; private set; }

        public event CompletedDelegate ScrapingCompleted;

        private ILog Log
        {
            get { return Logger.Log; }
        }

        private int PageNumber = 0;

        public _ScraperTemplate(string login, string password)
        {
            Log.Info("Starting new instance of {0}", GetType().Name);

            Login = login;
            Password = password;

            WatchdogElapsed += () => { EndScraping(false, "Request time out"); }; //signal for request timeout
        }


        public void DocumentCompletedHandler(object sender, System.Windows.Forms.WebBrowserDocumentCompletedEventArgs e)
        {
            WatchdogReset();

            Log.Info("Page {0} loaded", PageNumber);

            switch (PageNumber)
            {
                case 0:
                    Task.Run(() =>
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseFirstPage)
                        );
                    break;
                default:
                    string message = String.Format("{0}: Unknown page number {1}!", GetType().Name, PageNumber);

                    Log.Warn(message);

                    EndScraping(false, message);
                    return;
            }

            PageNumber++;
        }

        private bool ParseFirstPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseFirstPage), browser);
            }

            try
            {
                Log.Info("{0}: start parsing a page", GetType().Name);

                if (browser.Document == null)
                {
                    Log.Warn("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        /// <summary>
        /// Siganl all waiters that scrapping is completed
        /// </summary>
        /// <param name="succeeded"></param>
        /// <param name="result"></param>
        private void EndScraping(bool succeeded, string result = null)
        {
            try
            {
                WatchdogStop();

                if (ScrapingCompleted == null)
                    return;

                ScrapingCompleted(succeeded, result);
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("{0}: Error occured: {1}", this.GetType().Name, ex));
            }
        }
    }
}


