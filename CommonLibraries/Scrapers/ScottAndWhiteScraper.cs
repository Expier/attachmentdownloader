﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonLibraries.Common;

namespace CommonLibraries.Scrapers
{
    public class ScottAndWhiteScraper : Watchdog, IScraper
    {
        //first page constatns 
        private const string SUBMIT_BTN_NAME = "submitButton";

        //second page constatns
        private const string INPUT_PASSWORD_ID = "dialog:password";
        private const string INPUT_SUBMIT_ID = "dialog:continueButton";

        //third page constants
        private const string ATTACHMENT_SPAN_CLASS_NAME = "header-attachment-item";

        public string Login { get; private set; }

        public string Password { get; private set; }

        public event CompletedDelegate ScrapingCompleted;

        private ILog Log
        {
            get { return Logger.Log; }
        }

        private int PageNumber = 0;

        private int AttachmentsCount = 0;
        private int AttachmentsDownloaded = 0;

        private object syncObjDownload = new object();
        private object syncObjNewWindow = new object();

        private ManualResetEvent clickNextLink;

        public ScottAndWhiteScraper(string password)
        {
            Log.Info("Starting new instance of {0}", GetType().Name);

            Login = String.Empty;
            Password = password;

            WatchdogElapsed += () => { EndScraping(false, "Request time out"); }; //signal for request timeout
        }


        public void DocumentCompletedHandler(object sender, System.Windows.Forms.WebBrowserDocumentCompletedEventArgs e)
        {
            WatchdogReset();

            Log.Info("Page {0} loaded", PageNumber);

            switch (PageNumber)
            {
                case 0:
                    Task.Run(() =>
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseFirstPage)
                        );
                    break;

                case 1:
                    Task.Run(() =>
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseSecondPage)
                        );
                    break;

                case 2:
                    Task.Run(() =>
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseThirdPage)
                        );
                    break;

                default:
                    string message = String.Format("{0}: Unknown page number {1}!", GetType().Name, PageNumber);

                    Log.Warn(message);

                    EndScraping(false, message);
                    return;
            }

            PageNumber++;
        }

        private bool ParseThirdPage(WebBrowser browser)
        {
            try
            {
                Log.Info("{0}: start parsing a page", GetType().Name);

                List<HtmlElement> attachmentSpans = null;

                //searching for attachment span elements
                bool resultOk = false;
                while (!resultOk)
                {
                    attachmentSpans = ThirdPage_Action1(browser);

                    if (attachmentSpans == null || attachmentSpans.Count < 1)
                    {
                        Thread.Sleep(200);
                    }
                    else
                    {
                        resultOk = true;
                    }
                }

                //count of links for attachments fiound
                AttachmentsCount = attachmentSpans.Count;

                //handle new windows
                browser.NewWindow += browser_NewWindow;

                //handle download
                ((SHDocVw.WebBrowser)browser.ActiveXInstance).BeforeNavigate2 += ScottAndWhiteScraper_BeforeNavigate2;
                
                clickNextLink = new ManualResetEvent(false);
                
                //start clicking on attachment links
                foreach (var attachmentSpan in attachmentSpans)
                {
                    var links = attachmentSpan.GetElementsByTagName(HtmlTags.a.ToString());

                    //assume span contains only 1 link
                    links[0].InvokeMember("click");

                    //wait until all job is done on previous link
                    clickNextLink.Reset();
                    clickNextLink.WaitOne();
                }
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        private delegate List<HtmlElement> ThirdPageAction1Delegate(WebBrowser wevBrowser);

        private List<HtmlElement> ThirdPage_Action1(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                var elements = (List<HtmlElement>)browser.Invoke(new ThirdPageAction1Delegate(ThirdPage_Action1), browser);
                return elements;
            }

            try
            {
                if (browser.Document == null)
                {
                    Log.Warn("{0}: browser.Document is empty", GetType().Name);
                    return null;
                }

                return HtmlParser.FindElementByClassName(browser, HtmlTags.span, ATTACHMENT_SPAN_CLASS_NAME);
            }
            catch (Exception ex)
            {
                Log.Error("{0}: error occured: {1}", GetType().Name, ex);
                return null;
            }
        }

        void ScottAndWhiteScraper_BeforeNavigate2(object pDisp, ref object URL, ref object Flags, ref object TargetFrameName, ref object PostData, ref object Headers, ref bool Cancel)
        {
            lock (syncObjDownload)
            {
                try
                {
                    //cancel navigation
                    Cancel = true;

                    //reset watchdog
                    WatchdogReset();

                    //inceremt downloaded attachments count
                    ++AttachmentsDownloaded;

                    var byteArray = PostData as byte[];

                    //get web request prepared
                    HttpWebRequest webRequest = HtmlParser.PreapreWebRequest(URL.ToString(), byteArray);

                    using (Stream requeStream = webRequest.GetRequestStream())
                    {
                        //open connection
                        requeStream.Write(byteArray, 0, byteArray.Length); // Send the data.
                    }

                    //try to get file from response adn save it
                    HtmlParser.SaveFileFromResponse((HttpWebResponse) webRequest.GetResponse());

                    clickNextLink.Set();

                    if (AttachmentsDownloaded == AttachmentsCount)
                        EndScraping(true);
                }
                catch (Exception ex)
                {
                    Log.Error("{0}: error occured: {1}", GetType().Name, ex);
                }
            }
        }

        //Catchs NewWindow event and opens it in same tab
        void browser_NewWindow(object sender, System.ComponentModel.CancelEventArgs e)
        {
            lock (syncObjNewWindow)
            {
                e.Cancel = true;
                
                WebBrowser br = (WebBrowser)sender;
                
                br.Navigate(br.Url);
            }
        }

        private HttpWebRequest PreapreWebRequest(string url, byte[] postData)
        {
            try
            {

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);

                webRequest.CookieContainer = CookieAwareWebClient.GetUriCookieContainer(new Uri(url));
                webRequest.Method = WebRequestMethods.Http.Post;
                webRequest.UserAgent = "other";
                //"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2";
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.ProtocolVersion = HttpVersion.Version11;
                webRequest.AllowAutoRedirect = true;
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.ContentLength = postData.Length;

                return webRequest;
            }
            catch (Exception ex)
            {
                Log.Error("{0}: error occured: {1}", GetType().Name, ex);
                return null;
            }
        }

        private bool ParseSecondPage(WebBrowser browser)
        {
            try
            {
                Log.Info("{0}: start parsing a page", GetType().Name);

                while (!SecondPage_Action1(browser))
                {
                    Thread.Sleep(200);
                }

                while (!SecondPage_Action2(browser))
                {
                    Thread.Sleep(200);
                }

                while (!SecondPage_Action3(browser))
                {
                    Thread.Sleep(200);
                }

                return true;

            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        private bool SecondPage_Action1(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(SecondPage_Action1), browser);
            }

            try
            {
                var inputPassword = browser.Document.GetElementById(INPUT_PASSWORD_ID); //HtmlParser.FindElementByName(browser, HtmlTags.input, INPUT_PASSWORD_ID);

                if (inputPassword == null)
                {
                    Log.Warn("Unable to find element: <input id=\"{0}\"", INPUT_PASSWORD_ID);
                    return false;
                }

                inputPassword.Focus();
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: FirstPage_Action1 executed successfully", GetType().Name);
            return true;
        }

        private bool SecondPage_Action2(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(SecondPage_Action2), browser);
            }

            try
            {
                var inputPassword = browser.Document.GetElementById(INPUT_PASSWORD_ID);

                if (inputPassword == null)
                {
                    Log.Warn("Unable to find element: <input id=\"{0}\"", INPUT_PASSWORD_ID);
                    return false;
                }

                if (inputPassword.GetAttribute("type") != "password")
                    return false;

                inputPassword.SetAttribute("Value", Password);
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: FirstPage_Action2 executed successfully", GetType().Name);
            return true;
        }

        private bool SecondPage_Action3(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(SecondPage_Action3), browser);
            }

            try
            {
                var submitBtn = browser.Document.GetElementById(INPUT_SUBMIT_ID);
                //HtmlParser.FindElementByName(browser, HtmlTags.input, INPUT_SUBMIT_ID);

                if (submitBtn == null)
                {
                    Log.Warn("Unable to find element: <input id=\"{0}\"", INPUT_SUBMIT_ID);
                    return false;
                }

                submitBtn.InvokeMember("click");
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: FirstPage_Action3 executed successfully", GetType().Name);
            return true;
        }

        private bool ParseFirstPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseFirstPage), browser);
            }

            try
            {
                Log.Info("{0}: start parsing a page", GetType().Name);

                if (browser.Document == null)
                {
                    Log.Warn("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                var submitBtn = HtmlParser.FindElementByName(browser, HtmlTags.input, SUBMIT_BTN_NAME);

                if (submitBtn == null)
                {
                    Log.Warn("Unable to find element: <input name=\"{0}\"", SUBMIT_BTN_NAME);
                    return false;
                }

                submitBtn.InvokeMember("click");
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        /// <summary>
        /// Siganl all waiters that scrapping is completed
        /// </summary>
        /// <param name="succeeded"></param>
        /// <param name="result"></param>
        private void EndScraping(bool succeeded, string result = null)
        {
            try
            {
                WatchdogStop();

                if (ScrapingCompleted == null)
                    return;

                ScrapingCompleted(succeeded, result);
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("{0}: Error occured: {1}", this.GetType().Name, ex));
            }
        }
    }
}
