﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using CommonLibraries.Common;

namespace CommonLibraries.Scrapers
{
    public class PremierKidsScraper : Watchdog, IScraper
    {
        //first page constants
        private const string EMAIL_FIELD_ID = "email";
        private const string PASSWORD_FIELD_ID = "password";
        private const string LOGIN_BTN_ID = "btn_login";

        //second page constants
        private const string OPEN_BTN_ID = "ext-gen65";
        private const string EMPTY_GRID_CLASS = "x-grid-empty";

        ////third page consts 
        //private const string ATTACH_DIV_ID = "ext-gen162";

        private const string ATTACH_A_ONCLICK = "Inbox.readAttachFile";


        public string Login { get; private set; }
        public string Password { get; private set; }

        public event CompletedDelegate ScrapingCompleted;

        private ILog Log
        {
            get { return Logger.Log; }
        }

        private int PageNumber;

        private WebBrowser WebBrowser;

        public PremierKidsScraper(string login, string password)
        {
            Log.Info("Creating new instance of {0}", GetType().Name);

            Login = login;
            Password = password;

            WatchdogElapsed += () => { EndScraping(false, "Request time out"); }; //signal for request timeout
        }
        
        public void DocumentCompletedHandler(object sender, System.Windows.Forms.WebBrowserDocumentCompletedEventArgs e)
        {
            WatchdogReset();

            if (WebBrowser == null)
                WebBrowser = (WebBrowser)sender;

            switch (PageNumber)
            {
                case 0:
                    Task.Run(() =>
                    {
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseFirstPage);
                    });
                    break;

                case 1:
                    Task.Run(() =>
                    {
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseSecondPage);
                    });
                    break;

                default:
                    string message = String.Format("Unknown page number {0}!", PageNumber);

                    Log.Warn(message);

                    EndScraping(false, message);
                    break;
            }

            PageNumber++;
        }

        private bool ParseFirstPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseFirstPage), browser);
            }

            try
            {
#if DEBUG
                Log.Info("ParseFirstPage started");
#endif

                if (browser.Document == null)
                {
                    Log.Error("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                //find login field in document
                HtmlElement htmlElement = browser.Document.GetElementById(EMAIL_FIELD_ID);
                //HtmlParser.FindElementByName(browser, HtmlTags.input, EMAIL_FIELD_ID);

                if (htmlElement == null)
                {
                    Log.Warn("Unable to find element: <input id=\"{0}\"", EMAIL_FIELD_ID);
                    return false;
                }

                //htmlElement.InnerText = Login;
                htmlElement.SetAttribute("Value", Login);

                //find password field in document
                htmlElement = browser.Document.GetElementById(PASSWORD_FIELD_ID);

                if (htmlElement == null)
                {
                    Log.Warn("Unable to find element: <input id=\"{0}\"", PASSWORD_FIELD_ID);
                    return false;
                }

                //htmlElement.InnerText = Password;
                htmlElement.SetAttribute("Value", Password);

                //find login button in document
                htmlElement = browser.Document.GetElementById(LOGIN_BTN_ID);

                if (htmlElement == null)
                {
                    Log.Warn("Unable to find element: <input id=\"{0}\"", LOGIN_BTN_ID);
                    return false;
                }

                htmlElement.InvokeMember("click");

#if DEBUG
                Log.Info("ParseFirstPage completed");
#endif
            }
            catch (Exception ex)
            {
                Log.Error("{0}: exception occured: {1}", GetType().Name, ex);
                return false;
            }

            return true;
        }

        private bool ParseSecondPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseSecondPage), browser);
            }

            try
            {
#if DEBUG
                Log.Info("ParseSecondPage started");
#endif

                if (browser.Document == null)
                {
                    Log.Error("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                //find button to open email
                HtmlElement btnOpen = browser.Document.GetElementById(OPEN_BTN_ID);

                if (btnOpen == null)
                {
                    Log.Warn("Unable to find element: <button id=\"{0}\"", OPEN_BTN_ID);
                    return false;
                }

                //find empty div. If it present - email list is not loaded yet
                var elements = HtmlParser.FindElementByClassName(browser, HtmlTags.div, EMPTY_GRID_CLASS);
                HtmlElement emptyDiv = elements == null ? null : elements.FirstOrDefault();
                //browser.Document.GetElementById(EMAILS_LIST_ID);

                if (emptyDiv != null)
                {
                    Log.Warn("{0}: Email list is not loaded yet", GetType().Name);
                    return false;
                }

                btnOpen.InvokeMember("click");

                //start looking for attachment
                WatchdogReset();

                Task.Run(() =>
                {
                    HtmlParser.ParsePageInLoop(browser, ParseThirdPage);
                });

#if DEBUG
                Log.Info("ParseSecondPage completed");
#endif
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured: {1}", GetType().Name, ex);
                return false;
            }

            return true;
        }

        private bool ParseThirdPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseThirdPage), browser);
            }

            try
            {
#if DEBUG
                Log.Info("ParseThirdPage started");
#endif

                if (browser.Document == null)
                {
                    Log.Error("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                var links = browser.Document.GetElementsByTagName(HtmlTags.a.ToString());

                HtmlElement downloadLink = null;

                foreach (HtmlElement link in links)
                {
                    var onclickAction = HtmlParser.GetElementAttribute(link.OuterHtml, "onclick");
                    
                    if (String.IsNullOrEmpty(onclickAction))
                        continue;

                    if (onclickAction.StartsWith(ATTACH_A_ONCLICK))
                    {
                        //we've found link to download attachment 
                        downloadLink = link;
                        break;
                    }
                }

                if (downloadLink == null)
                    return false;

                //expecting for download dialog to appear
                (browser).Navigating += HtmlParser.BrowserDownloadEventHandler;

                HtmlParser.DownloadStarted += WatchdogReset; //reset Watchdog when download starts
                HtmlParser.DownloadCompleted += () => { EndScraping(true); }; //signal download completed successfully

                downloadLink.InvokeMember("click");

#if DEBUG
                Log.Info("Click action performed");
#endif


#if DEBUG
                Log.Info("ParseThirdPage completed");
#endif
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured: {1}", GetType().Name, ex);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Siganl all waiters that scrapping is completed
        /// </summary>
        /// <param name="succeeded"></param>
        /// <param name="result"></param>
        private void EndScraping(bool succeeded, string result = null)
        {
            try
            {
                WatchdogStop();

                if (ScrapingCompleted == null)
                    return;

                ScrapingCompleted(succeeded, result);
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("{0}: Error occured: {1}", this.GetType().Name, ex));
            }
        }
    }
}
