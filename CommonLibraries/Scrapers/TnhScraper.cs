﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonLibraries.Common;

namespace CommonLibraries.Scrapers
{
    public class TnhScraper : Watchdog, IScraper
    {
        private const string OUTPUT_FILE = "TNH.txt";
        private const string SEPARATION_CHAR = "|";

        //first page constants
        private const string INPUT_LOGIN_NAME = "Username";
        private const string INPUT_PASSWORD_NAME = "Password";

        //second page constants
        private const string INPUT_FROM_NAME = "From";
        private const string INPUT_TO_NAME = "To";
        private const string BTN_SELECT_TYPE = "button";
        private const string TABLE_DATA_CLASS = "table table-bordered table-striped";

        //common
        private const string BTN_SUBMIT_TYPE = "submit";

        public string Login { get; private set; }

        public string Password { get; private set; }

        public event CompletedDelegate ScrapingCompleted;

        private ILog Log
        {
            get { return Logger.Log; }
        }

        private int PageNumber = 0;

        public TnhScraper(string login, string password)
        {
            Log.Info("Starting new instance of {0}", GetType().Name);

            Login = login;
            Password = password;

            WatchdogElapsed += () => { EndScraping(false, "Request time out"); }; //signal for request timeout
        }


        public void DocumentCompletedHandler(object sender, System.Windows.Forms.WebBrowserDocumentCompletedEventArgs e)
        {
            WatchdogReset();

            Log.Info("Page {0} loaded", PageNumber);

            switch (PageNumber)
            {
                case 0:
                    Task.Run(() =>
                        HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseFirstPage)
                        );
                    break;

                case 1:
                    Task.Run(() =>
                            HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseSecondPage)
                            );
                    break;

                case 2:
                    //do nothing
                    break;

                default:
                    string message = String.Format("{0}: Unknown page number {1}!", GetType().Name, PageNumber);

                    Log.Warn(message);

                    EndScraping(false, message);
                    return;
            }

            PageNumber++;
        }

        /// <summary>
        /// Use to escape when writing text from source html to csv file
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private static string EncodeText(string text)
        {
            if (string.IsNullOrEmpty(text))
                return "\"\"";

            //decode html encoded chars
            var result = WebUtility.HtmlDecode(text);

            //escape " with ""
            result = result.Replace("\"", "\"\"");

            //remove new lines
            result = result.Replace(Environment.NewLine, String.Empty);

            //surround text with "
            result = String.Format("\"{0}\"", result);

            return result;
        }

        private bool GrabDataFromTable(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(GrabDataFromTable), browser);
            }

            try
            {
                Log.Info("{0}: start grabbing data table", GetType().Name);

                if (browser.Document == null)
                {
                    Log.Warn("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                //find data table
                var tblData = HtmlParser.FindElementsByAttribute(browser, HtmlTags.table, HtmlAttributes.ClassName, TABLE_DATA_CLASS);
                if (tblData == null || tblData.Count < 1)
                {
                    Log.Warn("Unable to find element: <table class=\"{0}\"", TABLE_DATA_CLASS);
                    return false;
                }

                if (GetRowsCount(tblData[0].InnerHtml) < 1)
                    return false;

                if (ParseTable(tblData[0].InnerHtml))
                {
                    Log.Info("Table parsed successfully");
                }
                else
                {
                    Log.Info("Table parsing completed with error");
                }

            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            return true;
        }

        private bool ParseTable(string innerHtml)
        {
            if (String.IsNullOrEmpty(innerHtml))
                return false;

            try
            {
                var cellPatern = @"<td[^>]*?>(.*?)</td>"; //@"<td[^>]*?>(.*?)";
                var headerPatern = @"<th[^>]*>(.*?)</th>"; //@"<th[^>]*>(.*?)";
                var rowPatern = @"<tr[^>]*>((?!</tr>).)*"; //@"<tr[^>]*>(.*?)";

                var outputFileFullPath = Path.Combine(Application.StartupPath, OUTPUT_FILE);

                Log.Info(String.Format("Output file: {0}", outputFileFullPath));

                Regex regex = new Regex(rowPatern, RegexOptions.Singleline);

                using (StreamWriter outputFile = new StreamWriter(outputFileFullPath, false))
                {

                    foreach (Match match in regex.Matches(innerHtml))
                    {
                        Regex cellRegex = new Regex(cellPatern, RegexOptions.Singleline);

                        Regex headerCellRegex = new Regex(headerPatern, RegexOptions.Singleline);

                        //td
                        foreach (Match cellMatch in cellRegex.Matches(match.Value))
                        {
                            outputFile.Write(String.Format("{0}{1}", EncodeText(cellMatch.Groups[1].Value), SEPARATION_CHAR));
                        }

                        //th
                        foreach (Match headerMatch in headerCellRegex.Matches(match.Value))
                        {
                            outputFile.Write(String.Format("{0}{1}", EncodeText(headerMatch.Groups[1].Value), SEPARATION_CHAR));
                        }

                        outputFile.Write(Environment.NewLine);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("{0} error occured: {1}", GetType().Name, ex);
                return false;
            }

            return true;
        }

        private int GetRowsCount(string innerHtml)
        {
            if (String.IsNullOrEmpty(innerHtml))
                return 0;

            try
            {
                var patern = @"<tr[^>].*>(.*?)"; //@"<tr[^>]*>(.*?)"; //@"<tr[^>]*>([^<]*)</tr>"; 

                Regex regex = new Regex(patern);
                return regex.Matches(innerHtml).Count;
            }
            catch (Exception ex)
            {
                Log.Error("{0} error occured: {1}", GetType().Name, ex);
                return 0;
            }
        }

        private bool ParseSecondPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseSecondPage), browser);
            }

            try
            {
                Log.Info("{0}: start parsing a page", GetType().Name);

                if (browser.Document == null)
                {
                    Log.Warn("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                //find input From
                var inputFrom = HtmlParser.FindElementByName(browser, HtmlTags.input, INPUT_FROM_NAME);
                if (inputFrom == null)
                {
                    Log.Warn("Unable to find element: <input name=\"{0}\"", INPUT_FROM_NAME);
                    return false;
                }

                //two weeks before now
                inputFrom.SetAttribute("Value", DateTime.Now.AddDays(-14).ToString("MM'/'dd'/'yyyy"));

                //find input To
                var inputTo = HtmlParser.FindElementByName(browser, HtmlTags.input, INPUT_TO_NAME);
                if (inputTo == null)
                {
                    Log.Warn("Unable to find element: <input name=\"{0}\"", INPUT_TO_NAME);
                    return false;
                }

                //set value to now
                inputTo.SetAttribute("Value", DateTime.Now.ToString("MM'/'dd'/'yyyy"));

                //find submit btn
                var submitBtn = HtmlParser.FindElementsByAttribute(browser, HtmlTags.button, HtmlAttributes.Type, BTN_SUBMIT_TYPE);
                if (submitBtn == null || submitBtn.Count < 1)
                {
                    Log.Warn("Unable to find element: <input type=\"{0}\"", BTN_SUBMIT_TYPE);
                    return false;
                }

                submitBtn[0].InvokeMember("click");

                bool result = false;

                WatchdogReset();

                Task.Run(() =>
                        HtmlParser.ParsePageInLoop(browser, GrabDataFromTable)
                        );
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        private bool ParseFirstPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseFirstPage), browser);
            }

            try
            {
                Log.Info("{0}: start parsing a page", GetType().Name);

                if (browser.Document == null)
                {
                    Log.Warn("{0}: browser.Document is empty", GetType().Name);
                    return false;
                }

                //find login input
                var inputLogin = HtmlParser.FindElementByName(browser, HtmlTags.input, INPUT_LOGIN_NAME);
                if (inputLogin == null)
                {
                    Log.Warn("Unable to find element: <input name=\"{0}\"", INPUT_LOGIN_NAME);
                    return false;
                }

                inputLogin.SetAttribute("Value", Login);

                //find password input
                var inputPassword = HtmlParser.FindElementByName(browser, HtmlTags.input, INPUT_PASSWORD_NAME);
                if (inputPassword == null)
                {
                    Log.Warn("Unable to find element: <input name=\"{0}\"", INPUT_PASSWORD_NAME);
                    return false;
                }

                inputPassword.SetAttribute("Value", Password);

                //find submit btn
                var submitBtn = HtmlParser.FindElementsByAttribute(browser, HtmlTags.button, HtmlAttributes.Type, BTN_SUBMIT_TYPE);
                if (submitBtn == null || submitBtn.Count < 1)
                {
                    Log.Warn("Unable to find element: <input type=\"{0}\"", BTN_SUBMIT_TYPE);
                    return false;
                }

                submitBtn[0].InvokeMember("click");
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        /// <summary>
        /// Siganl all waiters that scrapping is completed
        /// </summary>
        /// <param name="succeeded"></param>
        /// <param name="result"></param>
        private void EndScraping(bool succeeded, string result = null)
        {
            try
            {
                WatchdogStop();

                if (ScrapingCompleted == null)
                    return;

                ScrapingCompleted(succeeded, result);
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("{0}: Error occured: {1}", this.GetType().Name, ex));
            }
        }
    }
}
