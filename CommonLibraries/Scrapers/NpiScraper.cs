﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonLibraries.Common;

namespace CommonLibraries.Scrapers
{
    public class NpiScraper : Watchdog, IScraper
    {

        //First page constants
        private const string NpiRadioId = "ctl00_m_g_1e72c091_f48b_4e2d_89eb_9cc3e598ee76_ctl00_rblSource_2";
        private const string DeaRadioId = "ctl00_m_g_1e72c091_f48b_4e2d_89eb_9cc3e598ee76_ctl00_rblSource_1";

        private const string SearchFieldId = "ctl00_m_g_1e72c091_f48b_4e2d_89eb_9cc3e598ee76_ctl00_txtIndyID";
        private const string SubmitBtnName = "ctl00$m$g_1e72c091_f48b_4e2d_89eb_9cc3e598ee76$ctl00$cmdGetAMSIDs";


        #region Redundant
        public string Login { get; private set; }
        public string Password { get; private set; }
        #endregion

        public string Npi { get; private set; }

        public string Dea { get; private set; }

        public event CompletedDelegate ScrapingCompleted;

        private ILog Log
        {
            get { return Logger.Log; }
        }

        private int PageNumber = 0;

        public NpiScraper(string npi, string dea)
        {
            Log.Info("Starting new instance of {0}", GetType().Name);

            Npi = npi;
            Dea = dea;

            WatchdogElapsed += () => { EndScraping(false, "Request time out"); }; //signal for request timeout
        }


        public void DocumentCompletedHandler(object sender, System.Windows.Forms.WebBrowserDocumentCompletedEventArgs e)
        {
            WatchdogReset();

            Log.Info("Page {0} loaded", PageNumber);

            switch (PageNumber)
            {
                case 0:
                    //Task.Run(() =>
                    //    HtmlParser.ParsePageInLoop((WebBrowser)sender, ParseFirstPage)
                    //    );
                    break;
                default:
                    string message = String.Format("{0}: Unknown page number {1}!", GetType().Name, PageNumber);

                    Log.Warn(message);

                    EndScraping(false, message);
                    return;
            }

            PageNumber++;
        }

        private bool ParseFirstPage(WebBrowser browser)
        {
            //should check for InvokeRequired as may be called from different thread
            if (browser.InvokeRequired)
            {
                return (bool)browser.Invoke(new ParsePageDelegate(ParseFirstPage), browser);
            }

            try
            {
                Log.Info("{0}: start parsing a page", GetType().Name);
                
                //select RadioButton
                var radioBtn = browser.Document.GetElementById(String.IsNullOrEmpty(Npi) ? DeaRadioId : NpiRadioId);
                radioBtn.InvokeMember("Click");
                
                //insert ID to search by
                var searchInput = browser.Document.GetElementById(SearchFieldId);
                searchInput.SetAttribute("Value", String.IsNullOrEmpty(Npi) ? Dea : Npi);

                //click Submit
                var submitBtn = HtmlParser.FindElementsByAttribute(browser, HtmlTags.input, HtmlAttributes.Type,
                    "submit");
                submitBtn[0].InvokeMember("Click");
            }
            catch (Exception ex)
            {
                Log.Error("{0}: Exception occured {1}", GetType().Name, ex);
                return false;
            }

            Log.Info("{0}: Page parsed successfully", GetType().Name);
            return true;
        }

        /// <summary>
        /// Siganl all waiters that scrapping is completed
        /// </summary>
        /// <param name="succeeded"></param>
        /// <param name="result"></param>
        private void EndScraping(bool succeeded, string result = null)
        {
            try
            {
                WatchdogStop();

                if (ScrapingCompleted == null)
                    return;

                ScrapingCompleted(succeeded, result);
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("{0}: Error occured: {1}", this.GetType().Name, ex));
            }
        }
    }
}
