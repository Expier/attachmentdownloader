﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLibraries.Common;
using Ionic.Zip;

namespace CommonLibraries
{

    public struct UnziperParams
    {
        public string FromFolder { get; set; }
        public string ToFolder { get; set; }
        public string FileMask { get; set; }
        public string Password { get; set; }
        public bool Delete { get; set; }
        public bool PutTimeStamp { get; set; }
    }

    public static class Unziper
    {
        private static ILog Log
        {
            get { return Logger.Log; }
        }

        /// <summary>
        /// Runs Unziper
        /// </summary>
        /// <param name="unziperParams">Unziper's run parameters</param>
        /// <returns>True of runned successfully</returns>
        public static bool Run(UnziperParams unziperParams)
        {
            try
            {
                if (String.IsNullOrEmpty(unziperParams.FromFolder))
                {
                    Log.Warn("Unziper.Run: FromFolder param is empty!");
                    return false;
                }

                if (String.IsNullOrEmpty(unziperParams.FileMask))
                {
                    Log.Warn("Unziper.Run: FileMask param is empty!");
                    return false;
                }

                var filesToUnzip = Directory.GetFiles(unziperParams.FromFolder, unziperParams.FileMask);

                if (filesToUnzip.Count() < 1)
                {
                    Log.Warn("Unziper.Run: Files not found in {0} with mask {1}!", unziperParams.FromFolder, unziperParams.ToFolder);
                    return false;
                }

                foreach (var fileToUnzip in filesToUnzip)
                {
                    var result = Unzip(fileToUnzip, unziperParams.ToFolder, unziperParams.Password, unziperParams.PutTimeStamp);

                    if (result && unziperParams.Delete)
                    {
                        try
                        {
                            File.Delete(fileToUnzip);
                        }
                        catch (Exception ex)
                        {
                            Log.Error("Unziper.Run: error while deleting files. {0}", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Unziper.Run: error occured. {0}", ex);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Extracts ZIP archive to the destination folder
        /// </summary>
        /// <param name="zipFile">Zip file to extract data from</param>
        /// <param name="destinationPath">Folder to extract data to</param>
        /// <param name="password">Password if set</param>
        /// /// <param name="putTimeStamp">if TRUE updates extracted file's modified date</param>
        /// <returns>TRUE if data was successfully extracted</returns>
        public static bool Unzip(string zipFile, string destinationPath, string password = null, bool putTimeStamp = false)
        {
            try
            {
                using (ZipFile zip = ZipFile.Read(zipFile))
                {
                    zip.Password = password;

                    foreach (ZipEntry zipEntry in zip)
                    {
                        if (putTimeStamp)
                        {
                            zipEntry.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
                        }

                        zipEntry.Extract(destinationPath, ExtractExistingFileAction.OverwriteSilently);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Unziper.Unzip error occured: {0}", ex);
                return false;
            }

            return true;
        }
    }
}
