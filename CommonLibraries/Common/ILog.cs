﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibraries.Common
{
    public interface ILog
    {
        void Error(string format, params object[] arg);
        void Warn(string format, params object[] arg);
        void Info(string format, params object[] arg);

    }
}
