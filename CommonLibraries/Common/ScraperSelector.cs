﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using CommonLibraries.Scrapers;
using log4net.Appender;

namespace CommonLibraries.Common
{
    public static class ScraperSelector
    {
        //const to recognize file by content
        private const string Aetna = "aetna.com";
        private const string ScottAndWhite = "baylorhealth.edu";
        private const string Walgreens = "www.walgreens.com";

        //private static Logger Log;
        private static ILog Log
        {
            get { return Logger.Log; }
        }
        
        /// <summary>
        /// Selects proper scraper
        /// </summary>
        /// <returns>NULL if scraper is unknown</returns>
        public static IScraper Select(string[] args, out string url)
        {
            if (File.Exists(args[0]))
            {
                return SelecScraperByFile(args, out url);
            }
            else
            {
                return SelecScraperByUrl(args, out url);
            }
        }

        private static IScraper SelecScraperByFile(string[] args, out string url)
        {
            IScraper scraper = null;
            url = null;

            if (!CheckArgs(args, 1))
                return null;

            try
            {
                string pathToFile = null;
                
                string fileName = args[0];
                
                if (File.Exists(fileName))
                {
                    pathToFile = Path.GetFullPath(fileName);
                }

                if (String.IsNullOrEmpty(pathToFile))
                {
                    Log.Warn("SelecScraperByFile: Unable to find file: {0}", fileName);
                    return null;
                }

                //read file content 
                string fileContent;
                using (var streamReader = new StreamReader(pathToFile))
                {
                    fileContent = streamReader.ReadToEnd();
                }

                //check for keyword to identify scraper
                if (fileContent.Contains(Aetna))
                {
                    scraper = new AetnaScraper();
                }
                else if (fileContent.Contains(ScottAndWhite))
                {
                    if (!CheckArgs(args, 2))
                        return null;

                    scraper = new ScottAndWhiteScraper(args[1]);
                }
                else if (fileContent.Contains(Walgreens))
                {
                    scraper = new WalgreensScraper(args[1], args[2]);
                }

                url = String.Format("file:///{0}", pathToFile);
            }
            catch (Exception ex)
            {
                Log.Error("SelecScraperByFile: exception occured: {0}", ex);
            }

            return scraper;
        }

        private static IScraper SelecScraperByUrl(string[] args, out string url)
        {
            url = null;

            try
            {
                if (!CheckArgs(args, 1))
                    return null;

                url = args[0];

                var uri = new Uri(url);

                IScraper scraper = null;

                switch (uri.Host)
                {
                    case "shieldsrx.sharefile.com":
                        //scraper = new ShieldsScraper(args[1], args[2]);
                        scraper = new UmassScraper(args[1], args[2]);
                        break;

                    case "premierkidscare.access.seg.att.com":
                        scraper = new PremierKidsScraper(args[1], args[2]);
                        break;

                    case "securemail.providence.org":
                        scraper = new CredenaScraper(args[1], args[2]);
                        break;

                    case "game-message-portal.com":
                        scraper = new CostcoScraper(args[1], args[2]);
                        break;

                    case "www3.cignasecure.com":
                        scraper = new CignaScraper(args[1], args[2]);
                        break;

                    case "portal.tnhpharmacy.com":
                        scraper = new TnhScraper(args[1], args[2]);
                        break;

                    case "www.landvoice.com":
                        scraper = new LandvoiceScraper(args[1], args[2]);
                        break;

                    case "secure.amerisourcebergen.com":
                        scraper = new AmerisourceBergenScraper(args[1], args[2]);
                        break;

                    case "web1.zixmail.net":
                        scraper = new LdiScraper(args[1], args[2]);
                        break;

                    default:
                        Log.Warn("Unknown host: {0}", uri);
                        break;
                }

                return scraper;
            }
            catch (Exception ex)
            {
                Log.Error("SelectSecuredScraper: Exception occured: {0}", ex);
                return null;
            }
        }

        /// <summary>
        /// Checks needed arguments for URL scraper
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private static bool CheckArgs(string[] args, int expectedCount)
        {
            if (args.Count() < expectedCount)
            {
                Log.Warn("Arguments count is less than expected. Expected {0}. Actual {1}", expectedCount, args.Count());
                return false;
            }

            return true;
        }
    }
}
