﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using mshtml;
using SHDocVw;
using WebBrowser = System.Windows.Forms.WebBrowser;

namespace CommonLibraries.Common
{
    public enum HtmlTags
    {
        input,
        form,
        table,
        tr,
        div,
        a,
        span,
        li,
        button
    };

    public enum HtmlAttributes
    {
        Type,
        ClassName
    }

    /// <summary>
    /// Parse page dekegate
    /// </summary>
    /// <param name="wevBrowser">WebBrowser control</param>
    /// <returns>TRUE if parsed successfully</returns>
    public delegate bool ParsePageDelegate(WebBrowser wevBrowser);

    public static class HtmlParser//: IDisposable
    {
        //Defaults for async content generation timers, milliseconds
        private const int PARSE_PAGE_DELAY = 500;

        /// <summary>
        /// Fires when file download completed
        /// </summary>
        public static event Action DownloadCompleted;

        /// <summary>
        /// Fires when file download started
        /// </summary>
        public static event Action DownloadStarted;

        private static ILog Log
        {
            get { return Logger.Log; }
        }

        public static void ClearBrowserTempData()
        {
            try
            {
                //webBrowser.Navigate(
                //    "javascript:void((function(){var a,b,c,e,f;f=0;a=document.cookie.split('; ');for(e=0;e<a.length&&a[e];e++){f++;for(b='.'+location.host;b;b=b.replace(/^(?:%5C.|[^%5C.]+)/,'')){for(c=location.pathname;c;c=c.replace(/.$/,'')){document.cookie=(a[e]+'; domain='+b+'; path='+c+'; expires='+new Date((new Date()).getTime()-1e11).toGMTString());}}}})())");

                //Temporary Internet Files
                //System.Diagnostics.Process.Start("rundll32.exe", "InetCpl.cpl,ClearMyTracksByProcess 8");

                //Cookies()
                //System.Diagnostics.Process.Start("rundll32.exe", "InetCpl.cpl,ClearMyTracksByProcess 2");

                //History()
                //System.Diagnostics.Process.Start("rundll32.exe", "InetCpl.cpl,ClearMyTracksByProcess 1");

                ////Form(Data)
                //System.Diagnostics.Process.Start("rundll32.exe", "InetCpl.cpl,ClearMyTracksByProcess 16");

                ////Passwords
                //System.Diagnostics.Process.Start("rundll32.exe", "InetCpl.cpl,ClearMyTracksByProcess 32");

                ////Delete(All)
                //System.Diagnostics.Process.Start("rundll32.exe", "InetCpl.cpl,ClearMyTracksByProcess 255");

                ////Delete All – Also delete files and settings stored by add-ons
                System.Diagnostics.Process.Start("rundll32.exe", "InetCpl.cpl,ClearMyTracksByProcess 4351");

                ////int option = (int)3/* INTERNET_SUPPRESS_COOKIE_PERSIST*/;
                ////int* optionPtr = &option;

                //InternetSetOption(IntPtr.Zero, INTERNET_OPTION_END_BROWSER_SESSION, IntPtr.Zero, 0);
                //InternetSetOption(IntPtr.Zero, INTERNET_OPTION_SUPPRESS_BEHAVIOR, new IntPtr(3), 0);

                ////var cookiesDir = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.InternetCache));
                ////DeleteCookiesFiles(cookiesDir, domainName);
            }
            catch (Exception ex)
            {
                Log.Error("HtmlParser.ClearBrowserTempData: error occured: {0}", ex);
            }
        }

        private static void DeleteCookiesFiles(DirectoryInfo cookiesFolder, string domainName)
        {
            //get cookies files containing domain name 
            foreach (FileInfo file in cookiesFolder.GetFiles().Where(f => f.Name.Contains(domainName)))
            {
                try
                {
                    // Delete the file, ignoring any exceptions
                    file.Delete();
                }
                catch (Exception)
                {
                }
            }

            // For each folder in the specified folder
            foreach (DirectoryInfo subfolder in cookiesFolder.GetDirectories())
            {
                // Clear all the files in the folder
                DeleteCookiesFiles(subfolder, domainName);
            }
        }

        //[System.Runtime.InteropServices.DllImport("wininet.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto, SetLastError = true)]
        //public static extern bool InternetSetOption(int hInternet, int dwOption, IntPtr lpBuffer, int dwBufferLength);

        private const int INTERNET_OPTION_END_BROWSER_SESSION = 42;
        private const int INTERNET_OPTION_SUPPRESS_BEHAVIOR = 81;

        [DllImport("wininet.dll", SetLastError = true)]
        private static extern bool InternetSetOption(IntPtr hInternet, int dwOption, IntPtr lpBuffer, int lpdwBufferLength);


        /// <summary>
        /// Finds html element attribute value by its name
        /// </summary>
        /// <param name="element">html element string</param>
        /// <param name="attributeName">attribute name</param>
        /// <returns>NULL if element not found</returns>
        public static string GetElementAttribute(string element, string attributeName)
        {
            if (String.IsNullOrEmpty(element))
                return null;

            try
            {
                string pattern1 = String.Format("{0}='(.*)'", attributeName);
                string pattern2 = String.Format("{0}=\"(.*)\"", attributeName);

                Regex rgx = new Regex(pattern1, RegexOptions.IgnoreCase);

                var match = rgx.Match(element);

                string result = rgx.Match(element).Groups[1].Value;

                if (String.IsNullOrEmpty(result))
                {
                    rgx = new Regex(pattern2, RegexOptions.IgnoreCase);
                    result = rgx.Match(element).Groups[1].Value;
                }

                return result;
            }
            catch (Exception ex)
            {
                Log.Error("GetElementAttribute: exception occured: {0}", ex);
                return null;
            }
        }

        /// <summary>
        /// Try parse page untill TRUE is returned
        /// </summary>
        /// <param name="webBrowser"></param>
        /// <param name="pageParser"></param>
        public static void ParsePageInLoop(WebBrowser webBrowser, ParsePageDelegate pageParser, int delay = PARSE_PAGE_DELAY)
        {
#if DEBUG
            int i = 0;
#endif
            while (!pageParser(webBrowser))
            {

#if DEBUG
                Log.Info("Parsing page: iteration {0}", i);
                i++;
#endif

                Thread.Sleep(delay);
            }
        }

        /// <summary>
        /// Finds element by Tag Name and Element Name
        /// </summary>
        /// <param name="br"></param>
        /// <param name="tag"></param>
        /// <param name="elementName"></param>
        /// <returns></returns>
        public static HtmlElement FindElementByName(WebBrowser br, HtmlTags tag, string elementName)
        {
            if (br.Document == null) return null;

            var elementsByTag = br.Document.GetElementsByTagName(tag.ToString());
            if (elementsByTag.Count > 0)
            {
                var elementsByName = elementsByTag.GetElementsByName(elementName);
                if (elementsByName.Count <= 0)
                {
                    HandleError(br.Url.ToString(), ErrorTypes.ElemntNotFound, elementName);
                    return null;
                }

                return elementsByName[0];
            }
            else
            {
                HandleError(br.Url.ToString(), ErrorTypes.ElemntNotFound, elementName);
                return null;
            }
        }

        /// <summary>
        /// OBSOLETE. Use FindElementsByAttribute(...)
        /// </summary>
        /// <param name="br">WebBrowser control</param>
        /// <param name="tag">HTML tag</param>
        /// <param name="className">HTML class name</param>
        /// <returns>List of elements found</returns>
        public static List<HtmlElement> FindElementByClassName(WebBrowser br, HtmlTags tag, string className)
        {
            return FindElementsByAttribute(br, tag, HtmlAttributes.ClassName, className);
        }

        /// <summary>
        /// Finds elements by attribute type and name
        /// </summary>
        /// <param name="br">WebBrowser control</param>
        /// <param name="tag">HTML tag name</param>
        /// <param name="attribName">Attribute name</param>
        /// <param name="attribValue">Attribute value</param>
        /// <returns>List of elements found</returns>
        public static List<HtmlElement> FindElementsByAttribute(WebBrowser br, HtmlTags tag, HtmlAttributes htmlAttribute, string attribValue)
        {
            try
            {
                if (br.Document == null)
                {
                    HandleError(String.Empty, ErrorTypes.EmptyWebBrowserDocument, MethodBase.GetCurrentMethod().ToString());
                    return null;
                }

                var elementsByTag = br.Document.GetElementsByTagName(tag.ToString());

                return elementsByTag.Cast<HtmlElement>()
                            .Where(element => element.GetAttribute(htmlAttribute.ToString()) == attribValue)
                            .ToList();
            }
            catch (Exception ex)
            {
                Log.Error("HtmlParser.FindElementsByAttribute error occured: {0}", ex);
                return new List<HtmlElement>();
            }
        }

        public static void DwonloadHandlerWebrequest(object sender, WebBrowserNavigatingEventArgs e)
        {
            try
            {
                if (DownloadStarted != null)
                    DownloadStarted();

                if (String.Compare(e.Url.AbsolutePath, "false") == 0)
                    return;

                e.Cancel = true;

                WebBrowser br = ((WebBrowser)sender);

                if (br.Document == null)
                    return;

                var webRequest = (HttpWebRequest)WebRequest.Create(e.Url);
                webRequest.CookieContainer = CookieAwareWebClient.GetUriCookieContainer(e.Url);

                using (var response = webRequest.GetResponse())
                {
                    string fileName = response.Headers["Content-Disposition"] != null
                        ? response.Headers["Content-Disposition"].Replace("attachment; filename=", "").Replace("\"", "")
                        : response.Headers["Location"] != null
                            ? Path.GetFileName(response.Headers["Location"])
                            : Path.GetFileName(e.Url.ToString()).Contains('?') ||
                              Path.GetFileName(e.Url.ToString()).Contains('=')
                                ? Path.GetFileName(response.ResponseUri.ToString()).Replace(response.ResponseUri.Query, "")
                                : Guid.NewGuid().ToString();

                    string filePath = Application.StartupPath;

                    using (Stream fileStream = File.OpenWrite(Path.Combine(filePath, fileName)))
                    using (Stream webResponseStream = response.GetResponseStream())
                    {
                        byte[] buffer = new byte[8192];
                        int bytesRead;
                        while ((bytesRead = webResponseStream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            fileStream.Write(buffer, 0, bytesRead);
                        }
                    }
                }
            }
            finally
            {
                Log.Info("File download completed.");

                if (DownloadCompleted != null)
                    DownloadCompleted();
            }
        }

        /// <summary>
        /// Handles event of WebBrowser when file download is expected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void BrowserDownloadEventHandler(object sender, WebBrowserNavigatingEventArgs e)
        {
            if (String.Compare(e.Url.AbsolutePath, "false") == 0)
                return;

            e.Cancel = true;

            WebBrowser br = ((WebBrowser)sender);

            if (br.Document == null)
                return;

            CookieAwareWebClient webClient = new CookieAwareWebClient(CookieAwareWebClient.GetUriCookieContainer(e.Url));
            //webClient.Headers.Add(HttpRequestHeader.Cookie, br.Document.Cookie);
            webClient.DownloadDataCompleted += webClient_DownloadDataCompleted;

            if (DownloadStarted != null)
                DownloadStarted();

            Log.Info("Starting file download");

            webClient.DownloadDataAsync(e.Url);
        }

        static void webClient_DownloadDataCompleted(object sender, DownloadDataCompletedEventArgs e)
        {
            try
            {
                string filepath = Application.StartupPath;
                string filename = String.Empty;

                string headerContentDisposition = ((CookieAwareWebClient)sender).ResponseHeaders["content-disposition"];
                if (headerContentDisposition != null)
                {
                    filename = new ContentDisposition(headerContentDisposition).FileName;
                }
                else
                {
                    string headerLocation = ((CookieAwareWebClient)sender).ResponseHeaders["Location"];
                    if (headerLocation != null)
                    {
                        filename = Path.GetFileName(headerLocation);
                    }
                    else
                    {
                        filename = Guid.NewGuid().ToString();
                    }
                }

                File.WriteAllBytes(Path.Combine(filepath, filename), e.Result);

                Log.Info("File download completed.");

                if (DownloadCompleted != null)
                    DownloadCompleted();
            }
            catch (Exception ex)
            {
                Log.Warn("Failed to download file");
                Log.Error("webClient_DownloadDataCompleted: error occured: {0}", ex);
            }
        }

        //public delegate HtmlElement FindHtmlElemetDelegate();
        //        public static HtmlElement WaitForContetntGeneration(FindHtmlElemetDelegate actionDelegate, int ajaxDelay = AJAX_DELAY, int ajaxDelayStep = AJAX_DELAY_STEP)
        //        {
        //            int i = 0;
        //            HtmlElement htmlElement = null;

        //            while (i != ajaxDelay)
        //            {
        //                htmlElement = actionDelegate();

        //                if (htmlElement != null) //&& !String.IsNullOrEmpty(htmlElement.InnerHtml)
        //                {

        //                    break;
        //                }


        //                i += ajaxDelayStep;

        //#if DEBUG
        //                Console.WriteLine("Waiting for element to load {1} {0}", i, DateTime.Now);
        //#endif
        //                Thread.Sleep(1000);
        //                //Thread.Sleep(ajaxDelay);

        //                //Task.Delay(AjaxDelayStep);

        //                Application.DoEvents();
        //            }

        //            return htmlElement;
        //        }

        /// <summary>
        /// Exception handler
        /// </summary>
        /// <param name="url"></param>
        /// <param name="errorType"></param>
        /// <param name="message"></param>
        private static void HandleError(string url, ErrorTypes errorType, string message)
        {
            switch (errorType)
            {
                case ErrorTypes.ElemntNotFound:
                    Log.Warn(String.Format("Error on a page {0}: Element not found \"{1}\"", url, message));
                    break;

                case ErrorTypes.GeneralException:
                    Log.Error(String.Format("Exception occured: {0}", message));
                    break;

                case ErrorTypes.InsufficientPrivileges:
                    Log.Warn(String.Format("Insufficient privileges. Please run as Administrator. {1}Message: {0}", message, Environment.NewLine));
                    break;

                case ErrorTypes.EmptyWebBrowserDocument:
                    Log.Warn(String.Format("{0}: Document is empty", message));
                    break;
            }
        }

        /// <summary>
        /// Prepare WebRequest with Cookies and Headers
        /// </summary>
        /// <param name="url">Url of request</param>
        /// <param name="postData">POST data</param>
        /// <param name="headers">Headers</param>
        /// <returns>HttpWebrequest or null</returns>
        public static HttpWebRequest PreapreWebRequest(string url, byte[] postData, string headers = null, string navigatingUrl = null)
        {
            try
            {

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);

                webRequest.CookieContainer = CookieAwareWebClient.GetUriCookieContainer(new Uri(url));
                webRequest.Method = WebRequestMethods.Http.Post;
                webRequest.UserAgent = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)";
                //"other";
                //"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2";

                webRequest.AllowWriteStreamBuffering = true;
                webRequest.ProtocolVersion = HttpVersion.Version11;
                webRequest.AllowAutoRedirect = true;
                webRequest.ContentLength = postData.Length;
                webRequest.ContentType = "application/x-www-form-urlencoded";

                var headersLines =
                    headers.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

                foreach (var headerLine in headersLines)
                {
                    if (String.IsNullOrEmpty(headerLine))
                        continue;

                    int index = headerLine.IndexOf(":");

                    if (index < 0)
                        continue;

                    var headerType = headerLine.Substring(0, index);

                    switch (headerType)
                    {
                        case "Referer":
                            webRequest.Referer = headerLine.Substring(index + 2); //": "
                            break;

                        case "Content-Type":
                            webRequest.ContentType = headerLine.Substring(index + 2); //": "
                            break;

                        default:
                            Log.Warn("HtmlParser.PreapreWebRequest: Unhandled header type: {0}", headerType);
                            break;
                    }
                }

                if (String.IsNullOrEmpty(webRequest.Referer) && !String.IsNullOrEmpty(navigatingUrl))
                    webRequest.Referer = navigatingUrl;

                return webRequest;
            }
            catch (Exception ex)
            {
                Log.Error("HtmlParser.PreapreWebRequest error occured: {0}", ex);
                return null;
            }
        }

        /// <summary>
        /// Whe web response is expected to be file
        /// </summary>
        /// <param name="webResponse">HttpWebResponse to get data from</param>
        /// <returns>TRUE of succeeded</returns>
        public static bool SaveFileFromResponse(HttpWebResponse webResponse)
        {
            try
            {
                using (webResponse)
                {
                    var fileName = webResponse.Headers["Content-Disposition"]
                        .Replace("attachment; filename*=UTF-8'", String.Empty)
                        .Replace("attachment; filename=", String.Empty);

                    var filePath = Path.Combine(Application.StartupPath, fileName);

                    using (Stream fileStream = File.OpenWrite(filePath))
                    using (Stream webResponseStream = webResponse.GetResponseStream())
                    {
                        byte[] buffer = new byte[8192];
                        int bytesRead;
                        while ((bytesRead = webResponseStream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            fileStream.Write(buffer, 0, bytesRead);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("HtmlParser.SaveFileFromResponse error occured: {0}", ex);
                return false;
            }

            return true;
        }

        //public static string GetIFrame(WebBrowser browser, string id)
        //{
        //    try
        //    {
        //        ////// And the magic begins
        //        ////if (e.Url.AbsolutePath != new Uri("https://www.example.com/iframeyouneedurl.html").AbsolutePath)
        //        ////    return;

        //        ////webBrowser1.DocumentCompleted -= WebBrowserDocumentCompleted;

        //        ////string jCode = "var iframe = document.getElementsByName('iframe_element')[0]; var innerDoc = iframe.contentDocument || iframe.contentWindow.document; innerDoc.documentElement.innerHTML";
        //        //string jCode = String.Format(
        //        //    "var iframe = document.getElementsByName('{0}')[0]; var innerDoc = iframe.contentDocument || iframe.contentWindow.document; innerDoc.documentElement.innerHTML"
        //        //    , id);

        //        //string html = browser.Document.InvokeScript("eval", new object[] { jCode }).ToString();

        //        //return html;

        //        var iFrames = browser.Document.GetElementsByTagName("iframe");

        //        ((Gecko.DOM.GeckoIFrameElement)frames[0]).ContentDocument
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        // Returns null in case of failure.
        public static IHTMLDocument2 GetDocumentFromWindow(IHTMLWindow2 htmlWindow)
        {
            if (htmlWindow == null)
            {
                return null;
            }

            // First try the usual way to get the document.
            try
            {
                IHTMLDocument2 doc = htmlWindow.document;

                return doc;
            }
            catch (COMException comEx)
            {
                // I think COMException won't be ever fired but just to be sure ...
                if (comEx.ErrorCode != E_ACCESSDENIED)
                {
                    return null;
                }
            }
            catch (System.UnauthorizedAccessException)
            {
            }
            catch
            {
                // Any other error.
                return null;
            }

            // At this point the error was E_ACCESSDENIED because the frame contains a document from another domain.
            // IE tries to prevent a cross frame scripting security issue.
            try
            {
                // Convert IHTMLWindow2 to IWebBrowser2 using IServiceProvider.
                IServiceProvider sp = (IServiceProvider)htmlWindow;

                // Use IServiceProvider.QueryService to get IWebBrowser2 object.
                Object brws = null;
                sp.QueryService(ref IID_IWebBrowserApp, ref IID_IWebBrowser2, out brws);

                // Get the document from IWebBrowser2.
                IWebBrowser2 browser = (IWebBrowser2)(brws);

                return (IHTMLDocument2)browser.Document;
            }
            catch
            {
            }

            return null;
        }

        private const int E_ACCESSDENIED = unchecked((int)0x80070005L);
        private static Guid IID_IWebBrowserApp = new Guid("0002DF05-0000-0000-C000-000000000046");
        private static Guid IID_IWebBrowser2 = new Guid("D30C1661-CDAF-11D0-8A3E-00C04FC9E26E");

        // This is the COM IServiceProvider interface, not System.IServiceProvider .Net interface!
        [ComImport(), ComVisible(true), Guid("6D5140C1-7436-11CE-8034-00AA006009FA"),
        InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
        public interface IServiceProvider
        {
            [return: MarshalAs(UnmanagedType.I4)]
            [PreserveSig]
            int QueryService(ref Guid guidService, ref Guid riid, [MarshalAs(UnmanagedType.Interface)] out object ppvObject);
        }
    }
}
