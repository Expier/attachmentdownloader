﻿using System;
using System.Runtime.CompilerServices;
using log4net;

namespace CommonLibraries.Common
{
    public enum ErrorTypes
    {
        ElemntNotFound,
        GeneralException,
        InsufficientPrivileges,
        EmptyWebBrowserDocument,
    };

    public class Logger : ILog
    {

        public static Logger Log
        {
            get
            {
                if (_Logger == null)
                {
                    _Logger = new Logger();

                }

                return _Logger;
            }
        }
        private static Logger _Logger;

        private static log4net.ILog Log4netLog;

        private Logger()
        {
            Log4netLog = LogManager.GetLogger(typeof(CallConvThiscall));
            log4net.Config.XmlConfigurator.Configure();
        }
        //public static void Setup()
        //{
        //    Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();

        //    PatternLayout patternLayout = new PatternLayout();
        //    patternLayout.ConversionPattern = "%date [%thread] %-5level %logger - %message%newline";
        //    patternLayout.ActivateOptions();

        //    RollingFileAppender roller = new RollingFileAppender();
        //    roller.AppendToFile = false;
        //    roller.File = @"Logs\EventLog.txt";
        //    roller.Layout = patternLayout;
        //    roller.MaxSizeRollBackups = 5;
        //    roller.MaximumFileSize = "1GB";
        //    roller.RollingStyle = RollingFileAppender.RollingMode.Size;
        //    roller.StaticLogFileName = true;
        //    roller.ActivateOptions();
        //    hierarchy.Root.AddAppender(roller);

        //    MemoryAppender memory = new MemoryAppender();
        //    memory.ActivateOptions();
        //    hierarchy.Root.AddAppender(memory);

        //    hierarchy.Root.Level = Level.Info;
        //    hierarchy.Configured = true;
        //}

        //// Logger instance
        //public static ILog Log
        //{
        //    get
        //    {
        //        //Create and configure logger
        //        if (_logger == null)
        //        {
        //            _logger = LogManager.GetLogger(typeof(CallConvThiscall));
        //            log4net.Config.XmlConfigurator.Configure();
        //        }

        //        return _logger;
        //    }
        //}

        //private static ILog _logger;



        public void Error(string format, params object[] arg)
        {
#if DEBUG
            Console.WriteLine(format, arg);
#endif

            Log4netLog.Error(String.Format(format, arg));
        }

        public void Warn(string format, params object[] arg)
        {
#if DEBUG
            Console.WriteLine(format, arg);
#endif

            Log4netLog.Warn(String.Format(format, arg));
        }

        public void Info(string format, params object[] arg)
        {
#if DEBUG
            Console.WriteLine(format, arg);
#endif
            Log4netLog.Info(String.Format(format, arg));
        }
    }
}
