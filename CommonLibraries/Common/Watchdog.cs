﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms.VisualStyles;

namespace CommonLibraries.Common
{
    /// <summary>
    /// Class implements watchdog timer functionality
    /// </summary>
    public class Watchdog
    {
        //Watchdog timer interval, milliseconds
        private const int WATCHDOG_INTERVAL = 10 * 1000;

        public int WatchdogInterval
        {
            get
            {
                if (watchdogInterval == 0)
                    return WATCHDOG_INTERVAL;

                return watchdogInterval;
            }
            set
            {
                watchdogInterval = value;
                WatchdogTimer.Interval = value;
            }
        }

        private int watchdogInterval;

        private Timer WatchdogTimer;

        public event Action WatchdogElapsed;

        public Watchdog()
        {
            WatchdogTimer = new Timer(WATCHDOG_INTERVAL);
            WatchdogTimer.AutoReset = false;
            WatchdogTimer.Elapsed += WatchdogTimer_Elapsed;
        }

        public void WatchdogStart()
        {
            this.WatchdogTimer.Start();
        }

        public void WatchdogStop()
        {
            this.WatchdogTimer.Stop();
        }

        public void WatchdogReset()
        {
            this.WatchdogTimer.Stop();
            this.WatchdogTimer.Start();
        }

        void WatchdogTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (WatchdogElapsed != null)
                WatchdogElapsed();
        }
    }
}
