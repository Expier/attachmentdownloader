﻿using System.Windows.Forms;

namespace CommonLibraries.Common
{
    public delegate void CompletedDelegate(bool succeeded, object result);

    public interface IScraper
    {
        //string Url { get; }
        string Login { get; }
        string Password { get; }

        event CompletedDelegate ScrapingCompleted;

        void DocumentCompletedHandler(object sender, WebBrowserDocumentCompletedEventArgs e);
    }
}
