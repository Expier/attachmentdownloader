﻿using System;
using System.IO;
using CommonLibraries;
using Ionic.Zip;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    [TestClass]
    public class UnziperTests
    {
        private string TempFile;
        private string TempFolder;
        private string ZipFile;
        private string FileContent;
        private string Password;
        private DateTime FileTimeStamp;

        private void CreateZipWithPassword()
        {
            //create zip archive 
            TempFile = System.IO.Path.GetTempFileName();
            TempFolder = Path.GetDirectoryName(TempFile);
            ZipFile = Path.Combine(TempFolder, String.Format("{0}.zip", Path.GetFileNameWithoutExtension(TempFile)));

            FileContent = Guid.NewGuid().ToString();
            Password = Guid.NewGuid().ToString();

            //write content to file
            using (var writer = new StreamWriter(TempFile, false))
            {
                writer.WriteLine(FileContent);
            }

            //add file to archive
            using (ZipFile zip = new ZipFile())
            {
                zip.Password = Password;
                zip.Encryption = EncryptionAlgorithm.WinZipAes256;
                zip.AddFile(TempFile, "");
                zip.Save(ZipFile);
            }

            //delete temp file
            File.Delete(TempFile);
        }

        private void CreateZipWithoutPassword()
        {
            //create zip archive 
            TempFile = System.IO.Path.GetTempFileName();
            TempFolder = Path.GetDirectoryName(TempFile);
            ZipFile = Path.Combine(TempFolder, String.Format("{0}.zip", Path.GetFileNameWithoutExtension(TempFile)));

            FileContent = Guid.NewGuid().ToString();
            Password = Guid.NewGuid().ToString();

            //write content to file
            using (var writer = new StreamWriter(TempFile, false))
            {
                writer.WriteLine(FileContent);
            }
            
            //add file to archive
            using (ZipFile zip = new ZipFile())
            {
                zip.AddFile(TempFile, "");
                zip.Save(ZipFile);
            }

            //delete temp file
            File.Delete(TempFile);
        }

        private void TimestampPrepare()
        {
            //create zip archive 
            TempFile = System.IO.Path.GetTempFileName();
            TempFolder = Path.GetDirectoryName(TempFile);
            ZipFile = Path.Combine(TempFolder, String.Format("{0}.zip", Path.GetFileNameWithoutExtension(TempFile)));
            
            //change file's creation date to MinValue
            FileTimeStamp = DateTime.Now.AddDays(-10);

            File.SetCreationTime(TempFile, FileTimeStamp);
            File.SetLastAccessTime(TempFile, FileTimeStamp);
            File.SetLastWriteTime(TempFile, FileTimeStamp);

            //add file to archive
            using (ZipFile zip = new ZipFile())
            {
                zip.AddFile(TempFile, "");
                zip.Save(ZipFile);
            }

            //delete temp file
            File.Delete(TempFile);
        }

        [TestCleanup]
        public void Cleanup()
        {
            try { File.Delete(TempFile); }
            catch { }

            try { File.Delete(ZipFile); }
            catch { }
        }

        [TestMethod]
        public void UnzipWithPasswordTest()
        {
            //prepare method
            CreateZipWithPassword();

            //unzip file using password
            if (!Unziper.Unzip(ZipFile, TempFolder, Password))
            {
                Assert.Fail("Error on Unzip");
            }

            if (!File.Exists(TempFile))
            {
                Assert.Fail("File not found after unzipping");
            }

            using (var reader = new StreamReader(TempFile))
            {
                var content = reader.ReadToEnd().TrimEnd(Environment.NewLine.ToCharArray());

                if (!String.Equals(FileContent, content))
                {
                    Assert.Fail("File content differs after unzipping");
                }
            }
        }

        [TestMethod]
        public void UnzipWithWrongPasswordTest()
        {
            //prepare method
            CreateZipWithPassword();

            //unzip file using wrong password
            if (Unziper.Unzip(ZipFile, TempFolder, "wrongPassword"))
            {
                Assert.Fail("Error on Unzip with wrong password");
            }
        }

        [TestMethod]
        public void UnzipWithEmptyAndNullPasswordTest()
        {
            //prepare method
            CreateZipWithPassword();

            //unzip file using empty password
            if (Unziper.Unzip(ZipFile, TempFolder, String.Empty))
            {
                Assert.Fail("Error on Unzip with empty password");
            }

            //unzip file using null password
            if (Unziper.Unzip(ZipFile, TempFolder, null))
            {
                Assert.Fail("Error on Unzip with null password");
            }
        }

        [TestMethod]
        public void UnzipWithoutPasswordTest()
        {
            //prepare method
            CreateZipWithoutPassword();

            //unzip file using password
            if (!Unziper.Unzip(ZipFile, TempFolder))
            {
                Assert.Fail("Error on Unzip");
            }

            if (!File.Exists(TempFile))
            {
                Assert.Fail("File not found after unzipping");
            }

            using (var reader = new StreamReader(TempFile))
            {
                var content = reader.ReadToEnd().TrimEnd(Environment.NewLine.ToCharArray());

                if (!String.Equals(FileContent, content))
                {
                    Assert.Fail("File content differs after unzipping");
                }
            }
        }

        [TestMethod]
        public void PutTimestampTest()
        {
            //prepare method
            TimestampPrepare();
            
            //unzip file without modifying TimeStamp
            if (!Unziper.Unzip(ZipFile, TempFolder, null, false))
            {
                Assert.Fail("Error on Unzip");
            }

            if (!File.Exists(TempFile))
            {
                Assert.Fail("File not found after unzipping");
            }

            if (File.GetCreationTime(TempFile) != FileTimeStamp
                || File.GetLastAccessTime(TempFile) != FileTimeStamp
                || File.GetLastWriteTime(TempFile) != FileTimeStamp)
            {
                Assert.Fail("Wrong timestamp on a file!");
            }

            //unzip file with modifying TimeStamp
            if (!Unziper.Unzip(ZipFile, TempFolder, null, true))
            {
                Assert.Fail("Error on Unzip");
            }


            if (File.GetCreationTime(TempFile) == FileTimeStamp
                || File.GetLastAccessTime(TempFile) == FileTimeStamp
                || File.GetLastWriteTime(TempFile) == FileTimeStamp)
            {
                Assert.Fail("Wrong timestamp on a file!");
            }
        }

        [TestMethod]
        public void RunTest()
        {
            //prepare method
            CreateZipWithoutPassword();

            var unziperParams = new UnziperParams()
            {
                FromFolder = TempFolder,
                ToFolder = TempFolder,
                FileMask = String.Format("{0}.*", Path.GetFileNameWithoutExtension(TempFile)),
                Password = String.Empty,
                Delete = true,
                PutTimeStamp = false
            };

            if (!Unziper.Run(unziperParams))
            {
                Assert.Fail("Error while Unziper.Run");
            }

            if (!File.Exists(TempFile))
            {
                Assert.Fail("File not found after unzipping");
            }
            
            //Check if zip file was deleted
            if (File.Exists(ZipFile))
            {
                Assert.Fail("Zip file was not deleted!");
            }

            using (var reader = new StreamReader(TempFile))
            {
                var content = reader.ReadToEnd().TrimEnd(Environment.NewLine.ToCharArray());

                if (!String.Equals(FileContent, content))
                {
                    Assert.Fail("File content differs after unzipping");
                }
            }
        }
    }
}
