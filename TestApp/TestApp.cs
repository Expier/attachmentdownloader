﻿using System;
using System.Net;
using System.Windows.Forms;
using CommonLibraries.Common;

namespace TestApp
{
    public partial class FormTestApp : Form
    {
        public FormTestApp()
        {
            AttachmentScraper.Program.RegistrySetup();

            InitializeComponent();

            SetupScraper();
        }

        private IScraper scraper;

        private void btnGo_Click(object sender, EventArgs e)
        {
            this.webBrowserVisual.Navigate(Url);
        }

        private void scrapper_ScrapingCompleted(bool succeeded, object result)
        {
            MessageBox.Show("Scraping completed!");
        }

        #region Shileds

        private readonly string[] args = new[]
        {
            "https://shieldsrx.sharefile.com/d/bd1c3c9987174b178",
           "rxdata@armadahealthcare.com",
           "Arm2015!"
        };

        private string Url;

        //walgreens
        //"securedoc_20160311T081640.html",
        //"melisa.calderon@armadahealthcare.com",
        //"Armada99"

        ////"LDI"
        //"https://web1.zixmail.net/s/welcome.jsp?b=ldirx",
        //"valarie.moorer@armadahealthcare.com",
        //"Pea55$nut"

        ////AmerisourceBergen
        //"https://secure.amerisourcebergen.com/secureProject/jsp/Login.jsp",
        //"Em37ac",
        //"ahealth"

        //"NPI", 
        //"https://storefront.advantagems.com/genesis/providers/Pages/XREF.aspx",
        //"1104863737",
        //""

        ////Landvoice
        //"https://www.landvoice.com/expired/leads/csv?batch_id=11356795&tok=KEwyNDE1MUwKUydkOTJkZTg3ZmM3Y2RjNWMyMmY2ZWM3Y2JlZjI5MmY2ZicKcDAKdHAxCi4=",
        //"diana@vassileva.com", 
        //"tasheva"

        ////TNH
        //"https://portal.tnhpharmacy.com",
        //"ARMADAREPORTING",
        //"tnh123"

        ////Cigna
        //"https://www3.cignasecure.com/s/e?m=ABDE5gAuwMxuLd0ncKPTR3tp",
        //"melisa.calderon@armadahealthcare.com",
        //"mc932200"

        ////Costco
        //"https://game-message-portal.com/s/e?b=costco&",
        //"Anthony.Sosa@armadahealthcare.com", 
        //"Armada99*"

        ////ScottAndWhite
        // "SecureMessageAtt.html",
        //"mc932200!"

        ////Credena
        //private const string url = "https://securemail.providence.org/mc10/viewmessage.aspx?key=_xbongnn7y-lxy8xn52prcsdun1ajw ";
        //private const string login = "rxdata@armadahealthcare.com";
        //private const string pass = "mc932200!";

        ////Shields / UMASS
        //private const string url = "https://shieldsrx.sharefile.com/d/e9674e9b8793437ea";
        //private const string login = "rxdata@armadahealthcare.com";
        //private const string pass = "Arm2015!";

        ////PremierKids
        //private const string url = "https://premierkidscare.access.seg.att.com/eds/encryption/encryption_login.php?d=tmzS6bMB7n4-j0H3-kUbkd42uFsqQ-fvYdsJUSVvA5036YBbPXkZe-zFTC9_YP3tu4nVNjxPusNFUHmDv1v2Pl8p7lXZSNUnnKpvOBNDgLAnWkFd4yQERWhG7NfUI59rJs5XsLeYy-T6cAhwSW-kz_uRQM4NdpbekINvyVC1o2ifyAp0SMysXH3mM9a1q1Co&email=melisa.calderon%40armadahealthcare.com&time=1446236168&sender_email=maddie%40premierkidscare.com&h=df8d4dcfbd7e50b986e834094fc6ceab6d941be8a94a0addafea8dff0dc4614a";
        //private const string login = "melisa.calderon@armadahealthcare.com";
        //private const string pass = "mc932200!";

        private void SetupScraper()
        {
            //string uri;

            scraper = ScraperSelector.Select(args, out Url);

            if (scraper == null)
            {
                MessageBox.Show("Unable to select scraper");
                return;
            }

            //clear browser's cache before any navigation
            //#warning Temporary remed
            HtmlParser.ClearBrowserTempData();

            scraper.ScrapingCompleted += scrapper_ScrapingCompleted;
            this.webBrowserVisual.DocumentCompleted += scraper.DocumentCompletedHandler;
        }
        #endregion

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                //this.scraper.EndScraping(true);
                Application.ExitThread();
            }
            catch (Exception)
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //HtmlElement divContainer = this.webBrowserVisual.Document.GetElementById("ext-gen162");
            //MessageBox.Show(divContainer.InnerHtml);

            //this.webBrowserVisual.Navigate("file:///C:/Users/Expier/Downloads/message_zdm.html");
            //var url = String.Format("file:///{0}/{1}", Directory.GetCurrentDirectory(), "message_zdm.html");
            //this.webBrowserVisual.Navigate(url);
        }
    }
}
