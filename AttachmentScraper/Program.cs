﻿#if DEBUG
    #define DONT_CLOSE_APP
#endif

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security;
using System.Security.AccessControl;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonLibraries;
using CommonLibraries.Common;
using CommonLibraries.Scrapers;
using Microsoft.Win32;

namespace AttachmentScraper
{


    public class Program
    {
        private const string UnziperIdentifier = "unzip";

        //private static Logger Log;
        private static ILog Log
        {
            get { return Logger.Log; }
        }

        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                if (!RegistrySetup())
                    StopExecution();

                string url;

                if (String.Equals(args[0], UnziperIdentifier, StringComparison.CurrentCultureIgnoreCase))
                {
                    RunUnziper(args);
                    return;
                }
                
                //clear browser's cache before any navigation
//#warning Temporary remed
                HtmlParser.ClearBrowserTempData();

                IScraper scraper = ScraperSelector.Select(args, out url);

                if (scraper == null)
                {
                    Log.Warn("Main: unable to select scraper for args: {0}", args.ToString());
                    StopExecution();
                    return;
                }

                scraper.ScrapingCompleted += scraper_ScrapingCompleted;
                StartBrowserThread(url, scraper.DocumentCompletedHandler);
            }
            catch (Exception ex)
            {
                Log.Error("Exception occured: {0}", ex);
                StopExecution();
            }
        }

        /// <summary>
        /// Runs Unziper and stops program's execution
        /// </summary>
        /// <param name="args">Args passed to program</param>
        private static void RunUnziper(string[] args)
        {
            try
            {
                var unziperParams = new UnziperParams()
                {
                    FromFolder = args[1],
                    ToFolder = args[2],
                    FileMask = args[3],
                    Password = args[4].ToLower() == "none" ? String.Empty : args[4],
                    Delete = bool.Parse(args[5]),
                    PutTimeStamp = bool.Parse(args[5])
                };

                Unziper.Run(unziperParams);
            }
            catch (Exception ex)
            {
                Log.Error("RunUnziper error occured: {0}", ex);
            }
            
            StopExecution();
        }

        /// <summary>
        /// Sets up WebBrowther to run in modern IE compatibility mode (IE7 and later)
        /// NEEDS ADMIN PRIVILEGES
        /// </summary>
        public static bool RegistrySetup()
        {
            try
            {
                string installkey = @"SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION";
                string entryLabel = System.AppDomain.CurrentDomain.FriendlyName; //application name
                System.OperatingSystem osInfo = System.Environment.OSVersion;

                string version = osInfo.Version.Major.ToString() + '.' + osInfo.Version.Minor.ToString();
                uint editFlag = (uint)((version == "6.2") ? 0x2710 : 0x2328); // 6.2 = Windows 8 and therefore IE10
                //0x2af8 : 0x2328 //11
                //0x2710 : 0x2328 //10

                //64 bit or 32 bit only machine:
                SetRegistryKey(@"SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION",
                    entryLabel, editFlag);

                //32 bit on 64 bit machine:
                SetRegistryKey(@"SOFTWARE\Wow6432Node\Microsoft\Internet Explorer\MAIN\FeatureControl\FEATURE_BROWSER_EMULATION",
                    entryLabel, editFlag);

                return true;
            }
            catch (SecurityException securityException)
            {
                //Insufficient privileges exception
                Log.Warn("RegistrySetup: Insufficient privileges exception {0}", securityException);
                return false;
            }
            catch (Exception ex)
            {
                //general exception
                Log.Error("RegistrySetup: Exception occured: {0}", ex);
                return false;
            }
        }

        private static void SetRegistryKey(string installkey, string entryLabel, uint editFlag)
        {
            try
            {
                RegistryKey existingSubKey = Registry.LocalMachine.OpenSubKey(installkey, false); // readonly key
                //RegistryKey existingSubKey = Registry.CurrentUser.OpenSubKey(installkey, RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl); // readonly key

                if (existingSubKey != null) //&& existingSubKey.GetValue(entryLabel) == null
                {
                    //existingSubKey = Registry.LocalMachine.OpenSubKey(installkey, true); // writable key
                    existingSubKey = Registry.LocalMachine.OpenSubKey(installkey, RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl); // readonly key
                    existingSubKey.SetValue(entryLabel, unchecked((int)editFlag), RegistryValueKind.DWord);
                }
            }
            catch (Exception)
            {
            }
        }

        static void scraper_ScrapingCompleted(bool succeeded, object result)
        {
            StopExecution();
        }

        ///<summary>
        /// Stops WebBrowser's Thread
        /// </summary>
        private static void StopExecution()
        {
            try
            {
#if DONT_CLOSE_APP
                Console.WriteLine("Execution completed");
                Console.ReadKey();
#endif
                Application.Exit();
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("Error while stopping program{0} {1}", Environment.NewLine, ex));
            }
        }

        private static bool StartBrowserThread(string url, WebBrowserDocumentCompletedEventHandler documentCompletedEventHandler)
        {
            try
            {
                var th = new Thread(() =>
                {
                    WebBrowser webBrowser = new WebBrowser();
                    webBrowser.DocumentCompleted += documentCompletedEventHandler;
                    webBrowser.ScriptErrorsSuppressed = true;
                    webBrowser.Navigate(url);
                    Application.Run();
                });
                th.SetApartmentState(ApartmentState.STA);
                th.Start();


            }
            catch (Exception ex)
            {
                Log.Error("Exception while starting Browser Thread: {0}", ex);
                return false;
            }

            return true;
        }
    }
}
